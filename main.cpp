//
// This file is part of Rundata-qt.
//
// This program is free software licensed under the GNU GPL v3 or any later version.
// You can find a copy of this license in LICENSE.GPLv3 in the top directory of
// the source code.
//
// Copyright 2017 Vadim Frolov <fralik@gmail.com>
//
#include "PanelWindow.h"

#include "marble/MarbleDirs.h"

#include <QApplication>
#include <QString>
#include <QSslSocket>
#include <QDebug>
#include <QDir>

int main(int argc, char *argv[])
{
    Q_INIT_RESOURCE(myresources);

    QApplication a(argc, argv);

#ifdef Q_OS_MAC
    QCoreApplication::setOrganizationName(QStringLiteral("Vadim Frolov"));
    QCoreApplication::setOrganizationDomain(QStringLiteral("vadimfrolov.com"));
#else
    QCoreApplication::setOrganizationName(QStringLiteral("fralik Entertainment"));
    QCoreApplication::setOrganizationDomain(QStringLiteral("vadimfrolov.com"));
#endif
    QCoreApplication::setApplicationName(QStringLiteral("Rundata-qt"));

    if (QSslSocket::supportsSsl())
    {
        qDebug() << QStringLiteral("SSL lib ") << QSslSocket::sslLibraryVersionString();
    }

#ifdef Q_OS_MAC
    QDir pluginsDir(qApp->applicationDirPath());
    pluginsDir.cdUp();
    pluginsDir.cd(QStringLiteral("PlugIns/plugins"));
    Marble::MarbleDirs::setMarblePluginPath(pluginsDir.absolutePath());
#endif

    PanelWindow w;
    w.show();

    return a.exec();
}
