#ifndef ABOUTDIALOG_H
#define ABOUTDIALOG_H

#include <QDialog>
#include <QStringList>

namespace Ui {
    class AboutDialog;
}

class AboutDialog : public QDialog
{
    Q_OBJECT

public:
    enum InitialMode
    {
        ABOUT,
        LICENSES
    };

    explicit AboutDialog(InitialMode initialMode, QWidget *parent = Q_NULLPTR);
    ~AboutDialog();

private:
    void init(InitialMode initialMode);
    void buildIndex();
    void addLicense(int row, const QString& title, const QString& contents);
    QString readFile(const QString& path);

    static QStringList filterResourcePaths(const QStringList& paths);

    Ui::AboutDialog *ui = Q_NULLPTR;
    QStringList _indexContents;
    QString _licenseContents;
};

#endif // ABOUTDIALOG_H
