//
// This file is part of Rundata-qt.
//
// This program is free software licensed under the GNU GPL v3 or any later version.
// You can find a copy of this license in LICENSE.GPLv3 in the top directory of
// the source code.
//
// Copyright 2017 Vadim Frolov <fralik@gmail.com>
//
#include "InscriptionTreeItem.h"

#include <QDebug>

InscriptionTreeItem::InscriptionTreeItem(const QList<QVariant> &data, InscriptionTreeItem *parentItem, QObject *parent)
    : QObject(parent)
{
    _parentItem = parentItem;
    _itemData = data;
}

InscriptionTreeItem::~InscriptionTreeItem()
{
    qDeleteAll(_childItems);
}

void InscriptionTreeItem::appendChild(InscriptionTreeItem *child)
{
    _childItems.append(child);
}

void InscriptionTreeItem::appendItem(const QVariant &data)
{
    _itemData.append(data);
}

InscriptionTreeItem *InscriptionTreeItem::child(int row)
{
    return _childItems.value(row);
}

int InscriptionTreeItem::childCount() const
{
    return _childItems.count();
}

int InscriptionTreeItem::columnCount() const
{
    return _itemData.count();
}

QVariant InscriptionTreeItem::data(int column) const
{
    return _itemData.value(column);
}

// AKA childNumber
int InscriptionTreeItem::row() const
{
    if (_parentItem)
        return _parentItem->_childItems.indexOf(const_cast<InscriptionTreeItem*>(this));

    return 0;
}

InscriptionTreeItem *InscriptionTreeItem::parentItem()
{
    return _parentItem;
}

int InscriptionTreeItem::columnFromData(const QVariant data) const
{
    int col = -1;
    for (int i = 0; i < columnCount(); i++)
    {
        if (data.toString().toLower() == this->data(i).toString().toLower())
        {
            col = i;
            break;
        }
    }

    return col;
}

//bool InscriptionTreeItem::lost() const
//{
//    qDebug() << "Lost column " << _colLost;
//    //int col = 23; // this is BAD, but given the static nature of our data..
//    if (columnCount() >= _colLost)
//    {
//        QVariant d = data(_colLost);
//        if (d.userType() == QMetaType::Bool)
//            return d.toBool();
//    }
//    return false;
//}

//bool InscriptionTreeItem::newReading() const
//{
//    qDebug() << "New reading column " << _colNewReading;
//    for (int i = 0; i < columnCount(); i++)
//    {
//        qDebug() << QStringLiteral("Column %1: %2").arg(i).arg(data(i).toString());
//    }

//    //int col = 24; // this is BAD, but given the static nature of our data..
//    if (columnCount() >= _colNewReading)
//    {
//        QVariant d = data(_colNewReading);
//        if (d.userType() == QMetaType::Bool)
//            return d.toBool();
//    }

//    return false;
//}


