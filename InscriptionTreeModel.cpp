//
// This file is part of Rundata-qt.
//
// This program is free software licensed under the GNU GPL v3 or any later version.
// You can find a copy of this license in LICENSE.GPLv3 in the top directory of
// the source code.
//
// Copyright 2017 Vadim Frolov <fralik@gmail.com>
//
#include "InscriptionTreeModel.h"
#include "InscriptionTreeItem.h"

#include <QSqlDatabase>
#include <QSqlTableModel>
#include <QSqlRelationalTableModel>
#include <QSqlRecord>
#include <QStandardItemModel>
#include <QApplication>

InscriptionTreeModel::InscriptionTreeModel(QSqlDatabase &db, QObject *parent)
    : QAbstractItemModel(parent)
    , _displayableColumns(Q_NULLPTR)
    , _colLost(-1)
    , _colNewReading(-1)
{
    _db = &db;
    QList<QVariant> rootData;
    rootData << QStringLiteral("Signature") << QStringLiteral("Signature ID"); // can be treated as header
    _rootItem = new InscriptionTreeItem(rootData);

    this->setupModelData();
    _sqlModel = new QSqlQueryModel(this);

    // shall be called after setupModelData:
    _colLost = columnIndexByName(QStringLiteral("all_data.lost"));
    _colNewReading = columnIndexByName(QStringLiteral("all_data.new_reading"));

    // A map is not strictly necessary, but makes life easier by providing
    // the column name from DB
    _searchColumns[QStringLiteral("signature")] = tr("Signature");
    _searchColumns[QStringLiteral("all_data.found_location")] = tr("Found location");
    _searchColumns[QStringLiteral("all_data.parish")] = tr("Parish");
    _searchColumns[QStringLiteral("all_data.district")] = tr("District");
    _searchColumns[QStringLiteral("all_data.municipality")] = tr("Municipality");
    _searchColumns[QStringLiteral("all_data.current_location")] = tr("Current location");
    _searchColumns[QStringLiteral("all_data.original_site")] = tr("Original site");
    _searchColumns[QStringLiteral("all_data.rune_type")] = tr("Rune type");
    _searchColumns[QStringLiteral("all_data.dating")] = tr("Dating");
    _searchColumns[QStringLiteral("all_data.style")] = tr("Style");
    _searchColumns[QStringLiteral("all_data.carver")] = tr("Carver");
    _searchColumns[QStringLiteral("relTblAl_18.name")] = tr("Material type");
    _searchColumns[QStringLiteral("all_data.material")] = tr("Material");
    _searchColumns[QStringLiteral("all_data.object")] = tr("Object");
    _searchColumns[QStringLiteral("all_data.reference")] = tr("References");
    _searchColumns[QStringLiteral("all_data.norse_normalization")] = tr("Normalisation to Old West Norse");
    _searchColumns[QStringLiteral("all_data.scandinavian_normalization")] = tr("Normalisation to Old Scandinavian");
    _searchColumns[QStringLiteral("all_data.english_translation")] = tr("Translation to English");
    _searchColumns[QStringLiteral("all_data.transliteration")] = tr("Transliterated runic text");
    _searchColumns[QStringLiteral("crosses")] = tr("Cross form");
    _searchColumns[QStringLiteral("all_data.num_crosses")] = tr("Number of crosses");
    _searchColumns[QStringLiteral("all_data.additional")] = tr("Other");
    _searchColumns[QStringLiteral("hasAlternatives")] = tr("Alternative signature?");
    _searchColumns[QStringLiteral("all_data.lost")] = tr("Lost?");
    _searchColumns[QStringLiteral("all_data.new_reading")] = tr("New reading?");

    // Map of display columns, which are not part of _searchColumns. Used to display information
    // about the inscription to the user. It is possible to create it programmatically from
    // column names of 'all_data' DB table
    QMap<QString, QString> additionalDisplayColumns;
    additionalDisplayColumns[QStringLiteral("all_data.image_link")] = tr("Image link");

    QStringList defaultSelected; // list of column names that are selected for view by default
    defaultSelected << QStringLiteral("signature")
                    << QStringLiteral("all_data.transliteration")
                    << QStringLiteral("all_data.scandinavian_normalization")
                    << QStringLiteral("all_data.norse_normalization");
    QStringList humanNames;
    foreach (auto col, defaultSelected) {
       humanNames << _searchColumns.value(col);
    }

    initDisplayableColumns(defaultSelected, humanNames);
    addToDisplayableColumns(_searchColumns);
    addToDisplayableColumns(additionalDisplayColumns);
}

InscriptionTreeModel::~InscriptionTreeModel()
{
    delete(_rootItem);
}

QModelIndex InscriptionTreeModel::index(int row, int column, const QModelIndex &parent) const
{
//    if (parent.isValid() && parent.column() != 0)
//        return QModelIndex();
    if (!hasIndex(row, column, parent))
        return QModelIndex();

    auto parentItem = getItem(parent);
    auto childItem = parentItem->child(row);
    if (childItem)
        return createIndex(row, column, childItem);
    else
        return QModelIndex();
}

QModelIndex InscriptionTreeModel::parent(const QModelIndex &index) const
{
    if (!index.isValid())
        return QModelIndex();

    InscriptionTreeItem *childItem = static_cast<InscriptionTreeItem*>(index.internalPointer());
    auto parentItem = childItem->parentItem();

    if (parentItem == _rootItem)
        return QModelIndex();

    return createIndex(parentItem->row(), 0, parentItem);
}

int InscriptionTreeModel::rowCount(const QModelIndex &parent) const
{
    if (parent.column() > 0)
        return 0;

    auto parentItem = getItem(parent);
    return parentItem->childCount();
}

int InscriptionTreeModel::columnCount(const QModelIndex &parent) const
{
    return getItem(parent)->columnCount();

    // alternatively make it constant:
    // return 1;
}

QVariant InscriptionTreeModel::headerData(int section, Qt::Orientation orientation, int role) const
{
    if (role == Qt::DisplayRole && orientation == Qt::Horizontal)
        return _rootItem->data(section);

    return QVariant();
}

QMap<QString, QString> InscriptionTreeModel::searchableColumns() const
{
    return _searchColumns;
}

QAbstractItemModel *InscriptionTreeModel::displayableColumns() const
{
    return _displayableColumns;
}

QAbstractItemModel *InscriptionTreeModel::uniqueValues(const QString &columnName) const
{
    if (_searchColumns.contains(columnName))
    {
        _sqlModel->setQuery(QStringLiteral("SELECT DISTINCT %1 FROM all_data WHERE %1 NOT NULL AND %1 <> '' ORDER BY %1 COLLATE NOCASE;").arg(columnName));
    }
    else
    {
        _sqlModel->clear();
    }

    return _sqlModel;
}

QList<int> InscriptionTreeModel::signaturesForQuery(const QString sqlQuery)
{
    QList<int> res;
    QSqlQuery query(*_db);
    query.setForwardOnly(true);
    query.prepare(QStringLiteral("SELECT id FROM all_data WHERE %1").arg(sqlQuery));
    query.exec();
    while (query.next())
    {
        int id = query.value(0).toInt() - 1;
        res.push_back(id);
    }
    return res;
}

int InscriptionTreeModel::columnIndexByName(const QString &columnName) const
{
    // Also possible to cache this data into a map, so that we do not need
    // to search for it. Remember, that in general our data is static.
    return _rootItem->columnFromData(columnName);
}

QList<QVector<QStringList> > InscriptionTreeModel::crosses(const QModelIndex &signatureIndex) const
{
    QList<QVector<QStringList> > results;
    int metaId = data(this->index(signatureIndex.row(), 2, signatureIndex.parent())).toInt();

    QSqlQuery query(*_db);
    query.prepare(QStringLiteral("SELECT crosses_meta.cross_id as crossId, cross_forms.name, cross_forms.group_id FROM crosses_meta JOIN crosses, cross_forms ON crosses.cross_id = crosses_meta.cross_id AND cross_forms.id = crosses.form_id WHERE crosses_meta.meta_id = :id ORDER BY crossId"));
    query.bindValue(QStringLiteral(":id"), metaId);
    query.exec();
    if (query.size() == 0)
        return results;

    int lastCrossId = -1;
    while (query.next())
    {
        int crossId = query.value(0).toInt();
        if (lastCrossId != crossId)
        {
            QVector<QStringList> groupForms(8);
            results.append(groupForms);
            lastCrossId = crossId;
        }
        QString formName = query.value(1).toString();
        int groupId = query.value(2).toInt();

        QVector<QStringList> &cross = results.last();
        QStringList &group = cross[groupId];
        group << formName;
        //results.last().replace(groupId, results.last().value(groupId) << formName);
    }
    return results;
}

QVariant InscriptionTreeModel::data(const QModelIndex &index, int role) const
{
    if (!index.isValid())
        return QVariant();

    auto item = getItem(index);

    if (role == Qt::DisplayRole || role == Qt::EditRole)
    {
        return item->data(index.column());
    }

    if (role == Qt::ToolTipRole)
    {
        if (isLost(item) && hasNewReading(item))
            return QString(tr("Signature is lost and has a new reading"));
        else if (isLost(item))
            return QString(tr("Signature is lost"));
        else if (hasNewReading(item))
            return QString(tr("Signature has a new reading"));
    }

    if (isLost(item) && (role == Qt::DecorationRole || role == Qt::SizeHintRole))
    {
        QIcon icon = QApplication::style()->standardIcon(QStyle::SP_DialogCancelButton);
        if (role == Qt::DecorationRole)
            return icon;
        if (role == Qt::SizeHintRole)
            return QSize(16, 16);
    }

    if (role == InscriptionTreeModel::IsLostRole)
    {
        return isLost(item);
    }

    if (role == InscriptionTreeModel::NewReadingRole)
    {
        return hasNewReading(item);
    }

    return QVariant();
}

Qt::ItemFlags InscriptionTreeModel::flags(const QModelIndex &index) const
{
    if (!index.isValid())
        return 0;

    return QAbstractItemModel::flags(index);
}

InscriptionTreeItem *InscriptionTreeModel::getItem(const QModelIndex &index) const
{
    if (index.isValid()) {
        auto item = static_cast<InscriptionTreeItem*>(index.internalPointer());
        if (item)
            return item;
    }
    return _rootItem;
}

bool InscriptionTreeModel::isLost(InscriptionTreeItem *item) const
{
    if (item == Q_NULLPTR || _colLost == -1)
    {
        return false;
    }

    QVariant d = item->data(_colLost);
    if (d.userType() == QMetaType::Bool)
        return d.toBool();

    return false;
}

bool InscriptionTreeModel::hasNewReading(InscriptionTreeItem *item) const
{
    if (item == Q_NULLPTR || _colNewReading == -1)
        return false;

    QVariant d = item->data(_colNewReading);
    return d.toBool();
}

void InscriptionTreeModel::initDisplayableColumns(const QStringList &dbNames, const QStringList &humanNames)
{
    // 4 roles:
    // 1. column internal key;
    // 3. real flag indicates whether row is selected or not;
    // 4. temporary flag, used during field selection, we need for the case when user rejects selection dialog.
    // 5. flag that controls if column names should be presented as well as data. If false, only data is shown.
    _displayableColumns = new QStandardItemModel(dbNames.size(), 1, this);
    for (int i = 0; i < dbNames.size(); ++i)
    {
        auto item = new QStandardItem(humanNames[i]);
        item->setData(dbNames[i], InscriptionTreeModel::ColumnNameRole);
        item->setData(true, InscriptionTreeModel::SelectedRole);
        item->setData(true, InscriptionTreeModel::TemporarySelectedRole);
        item->setData(false, InscriptionTreeModel::ShowColumnNameRole);

        _displayableColumns->setItem(i, 0, item);
    }
}

void InscriptionTreeModel::addToDisplayableColumns(const QMap<QString, QString> &map)
{
    int row = 0;
    if (!_displayableColumns)
    {
        // 4 roles:
        // 1. column internal key;
        // 3. real flag indicates whether row is selected or not;
        // 4. temporary flag, used during field selection, we need for the case when user rejects selection dialog.
        // 5. flag that controls if column names should be presented as well as data. If false, only data is shown.
        _displayableColumns = new QStandardItemModel(map.count(), 1, this);
    }
    else
    {
        row = _displayableColumns->rowCount();
        //_displayableColumns->insertRows(row, map.count());
    }

    QMapIterator<QString, QString> it(map);
    while (it.hasNext())
    {
        it.next();

        bool selected = false;
        if (_displayableColumns->findItems(it.value()).size() > 0)
        {
            continue;
        }
        auto item = new QStandardItem(it.value());
        item->setData(it.key(), InscriptionTreeModel::ColumnNameRole);
        item->setData(selected, InscriptionTreeModel::SelectedRole);
        item->setData(selected, InscriptionTreeModel::TemporarySelectedRole);
        item->setData(false, InscriptionTreeModel::ShowColumnNameRole);

        _displayableColumns->insertRow(row++, item);
    }
}

void InscriptionTreeModel::dumpModel(QAbstractItemModel *model, const QString &additional)
{
    if (!additional.isEmpty())
        qDebug() << additional;

    for (int i = 0; i < model->rowCount(); ++i)
    {
        qDebug() << QStringLiteral("%1 - %2").arg(i).arg(model->data(model->index(i, 0)).toString());
    }
}

void InscriptionTreeModel::setupModelData()
{
    const QScopedPointer<QSqlTableModel> signaturesModel(new QSqlTableModel(Q_NULLPTR, *_db));
    const QScopedPointer<QSqlRelationalTableModel> allInfo(new QSqlRelationalTableModel(Q_NULLPTR, *_db));

    allInfo->setTable(QStringLiteral("all_data"));
    allInfo->setRelation(18, QSqlRelation(QStringLiteral("material_types_en"),
                    QStringLiteral("material_type_id"), QStringLiteral("name")));
    allInfo->select();
    while (allInfo->canFetchMore())
        allInfo->fetchMore();

    bool needHeader = false;
    if (_rootItem->columnCount() == 2)
        needHeader = true;

    int numRows = allInfo->rowCount();
    // map between signature parent_id in the DB and row ID in SignatureTreeModel
    QMap<int, int> parentIds;

    QList<InscriptionTreeItem*> topItems;
    for (int i = 0; i < allInfo->rowCount(); i++)
    {
        QSqlRecord curRecord = allInfo->record(i);
        int signatureId = curRecord.value(1).toInt();
        QString signature = curRecord.value(0).toString();

        QVariantList treeItem;
        treeItem << signature;
        treeItem << signatureId;
        for (int j = 2; j < curRecord.count(); ++j)
        {
            treeItem << curRecord.value(j);
            if (needHeader)
            {
                _rootItem->appendItem(curRecord.fieldName(j));
            }
        }
        needHeader = false;
        auto newSign = new InscriptionTreeItem(treeItem, _rootItem);
        topItems << newSign;
        _rootItem->appendChild(newSign);

        parentIds[signatureId] = i;
    }

    // The table has 3 columns: internal ID; signature string representation; parent ID
    signaturesModel->setTable(QStringLiteral("signatures"));
    signaturesModel->setEditStrategy(QSqlTableModel::OnManualSubmit);
    signaturesModel->setFilter(QStringLiteral("parent_id IS NOT NULL"));
    signaturesModel->select();
    while (signaturesModel->canFetchMore())
        signaturesModel->fetchMore();
    numRows = signaturesModel->rowCount();
    for (int i = 0; i < signaturesModel->rowCount(); ++i)
    {
        QSqlRecord curRecord = signaturesModel->record(i);
        QString signature = curRecord.value(1).toString();

        int dbId = curRecord.value(2).toInt();
        int modelId = parentIds.value(dbId, -1);

        if (modelId == -1)
        {
            qDebug() << "something went wrong";
            continue;
        }

        QVariantList treeItem;
        treeItem << signature;
        //treeItem << curRecord.value(1).toInt(); // signature id

        auto signatureParent = topItems.at(modelId);
        signatureParent->appendChild(new InscriptionTreeItem(treeItem, signatureParent));
    }
}

