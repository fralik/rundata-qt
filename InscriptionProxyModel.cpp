//
// This file is part of Rundata-qt.
//
// This program is free software licensed under the GNU GPL v3 or any later version.
// You can find a copy of this license in LICENSE.GPLv3 in the top directory of
// the source code.
//
// Copyright 2017 Vadim Frolov <fralik@gmail.com>
//
#include "InscriptionProxyModel.h"

#include <QDebug>

InscriptionProxyModel::InscriptionProxyModel(QObject *parent)
    : QSortFilterProxyModel(parent)
    , _enabled(false)
{

}

void InscriptionProxyModel::setValidIndices(const QList<int> indices)
{
    _validIds = indices;
    invalidateFilter();
    emit filterChanged();
}

void InscriptionProxyModel::setEnabled(bool enabled)
{
    _enabled = enabled;
    invalidateFilter();
    emit filterChanged();
}

bool InscriptionProxyModel::filterAcceptsRow(int source_row, const QModelIndex &source_parent) const
{
    if (!_enabled)
    {
        return true;
    }
    if (_validIds.empty())
        return false;

    // Always show children
    if (source_parent.isValid())
        return true;

    return _validIds.contains(source_row);
}
