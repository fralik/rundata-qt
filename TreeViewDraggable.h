//
// This file is part of Rundata-qt.
//
// This program is free software licensed under the GNU GPL v3 or any later version.
// You can find a copy of this license in LICENSE.GPLv3 in the top directory of
// the source code.
//
// Copyright 2017 Vadim Frolov <fralik@gmail.com>
//
#ifndef TREEVIEW_DRAGGABLE_H
#define TREEVIEW_DRAGGABLE_H

#include <QTreeView>
#include <QDropEvent>

/**
 * @brief The TreeViewDraggable class enables drag and drop support in QTreeView in order to move rows around.
 *
 * The underlying model shall:
 * 1. implement moveRows method;
 * 2. return Qt::MoveAction from supportedDropActions;
 * 3. Return appropriate item flags, including Qt::ItemIsDragEnabled and Qt::ItemIsDropEnabled.
 */
class TreeViewDraggable : public QTreeView
{
    Q_OBJECT
public:
    explicit TreeViewDraggable(QWidget *parent = Q_NULLPTR);

protected:
    void dropEvent(QDropEvent *event);
};

#endif // TREEVIEW_DRAGGABLE_H
