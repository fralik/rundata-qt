//
// This file is part of Rundata-qt.
//
// This program is free software licensed under the GNU GPL v3 or any later version.
// You can find a copy of this license in LICENSE.GPLv3 in the top directory of
// the source code.
//
// Copyright 2017 Vadim Frolov <fralik@gmail.com>
//
#ifndef CROSSESSQLTABLE_H
#define CROSSESSQLTABLE_H

#include <QObject>
#include <QSqlTableModel>

/**
 * @brief A QSqlTableModel that shows images for data in one column
 *
 * This class can be used to show png images by file names stored in
 * the DB. Path to images is hard-coded as ':/images/crosses/<fileName>.png'.
 */
class CrossesSqlTable : public QSqlTableModel
{
    Q_OBJECT

public:
    explicit CrossesSqlTable(QObject *parent = Q_NULLPTR, QSqlDatabase db = QSqlDatabase());

    // QAbstractItemModel interface
public:
    QVariant data(const QModelIndex &index, int role) const Q_DECL_OVERRIDE;
};

#endif // CROSSESSQLTABLE_H
