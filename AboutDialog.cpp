#include "AboutDialog.h"
#include "ui_AboutDialog.h"
#include <QDebug>
#include <QFile>
#include <QApplication>
#include <QDir>
#include <QtMath>

#include "marble/MarbleGlobal.h"

AboutDialog::AboutDialog(InitialMode initialMode, QWidget *parent)
    : QDialog(parent)
    , ui(new Ui::AboutDialog)
{
    init(initialMode);
}

AboutDialog::~AboutDialog()
{
    delete ui;
}

void AboutDialog::init(InitialMode initialMode)
{
    ui->setupUi(this);
    ui->leftIcon->setPixmap(QPixmap(QStringLiteral(":/rundata-256.png")));

    ui->tabWidget->setCurrentWidget(initialMode == ABOUT ? ui->about : ui->license);

    auto rundataVersion = QStringLiteral(RUNDATA_VERSION_STRING);
    auto qtVersion = QStringLiteral(QT_VERSION_STR);

    // About
    auto newLabelValue = ui->aboutLabel->text().arg(rundataVersion);
    ui->aboutLabel->setText(newLabelValue);

    // Licenses
    _licenseContents = QString();
    int row = 1;

    // title - file name
    QMap<QString, QString> licenses;
    licenses[QStringLiteral("Rundata-qt v%1 [GPL v3]").arg(rundataVersion)] = QStringLiteral("LICENSE.GPLv3");
    licenses[QStringLiteral("Qt v%1 [GPL v3]").arg(qtVersion)] = QStringLiteral("Qt.GPLv3");
    licenses[QStringLiteral("Marble Library v%1 [LGPL v2.1]").arg(Marble::MARBLE_VERSION_STRING)] = QStringLiteral("Marble.LGPLv21");

    QString title;
    QMapIterator<QString, QString> it(licenses);
    QDir curDir(qApp->applicationDirPath());
#ifdef Q_OS_MAC
    curDir.cd(QStringLiteral("../Resources"));
#endif
    curDir.cd(QLatin1String("licenses"));
    while (it.hasNext())
    {
        it.next();
        title = it.key();
        auto licenseFile = curDir.absoluteFilePath(it.value());
        if (QFile::exists(licenseFile))
        {
            addLicense(row++, title, readFile(licenseFile));
        }
    }

    buildIndex();

    ui->licenseEdit->setHtml(_licenseContents);
    _indexContents.clear();
    _licenseContents.clear();
}

void AboutDialog::buildIndex()
{
    static const QString entryTemplate = QStringLiteral("<li>%1</li>");
    QStringList entries;
    foreach (auto idx, _indexContents) {
       entries += entryTemplate.arg(idx);
    }

    _licenseContents.prepend(tr("<h3>Table of contents:</h3><ol>%2</ol>").arg(entries.join(QString())));
}

void AboutDialog::addLicense(int row, const QString& title, const QString& contents)
{
    auto escapedTitle = title.toHtmlEscaped();
    auto rowNum = QString::number(row);
    _licenseContents += QLatin1String("<h3>") + rowNum + QLatin1String(". ") + escapedTitle + QLatin1String("</h3>");
    _licenseContents += QLatin1String("<pre>") + contents.toHtmlEscaped() + QLatin1String("</pre>");
    _indexContents += escapedTitle;
}

QString AboutDialog::readFile(const QString& path)
{
    QFile file(path);
    if (!file.open(QIODevice::ReadOnly | QIODevice::Text))
    {
        qCritical() << "Error opening" << file.fileName();
        return QString::null;
    }
    auto contents = QString::fromLatin1(file.readAll());
    file.close();
    return contents;
}

QStringList AboutDialog::filterResourcePaths(const QStringList& paths)
{
    QStringList output;
    for (const QString& path : paths)
    {
        if (path.startsWith(QLatin1Char(':')))
            continue;

        output << path;
    }
    return output;
}
