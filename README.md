WARNING: Deprecated projected
=============================

This project is deprecated in favour of a web version [Rundata-net](https://gitlab.com/fralik/rundata-net). Visit [www.rundata.info](https://www,rundata.info) to work with Rundata in your browser!

Rundata-qt was fun. I might come back to it in the future if I can get rid of KDE Marble dependency. Having Qt-only app makes deployment so much easier.


Rundata-qt
----------

This is a client program for [Scandinavian Runic-text Data Base](http://www.nordiska.uu.se/forskn/samnord.htm). It can be viewed as a next version of original Rundata client. The original client supported only Windows with rather limited support of Linux and almost no support of macOS. The intention behind Rundata-qt is to bring support for macOS users and refresh the look of the original program.

Rundata-qt stores information about runic inscriptions in a SQLITE database. The program itself is just a wrapper for database queries with graphical user interface.

Screenshots
-----------

Here is the main screen:
![Rundata-qt](doc/main_screen_web.png "Main screen")

Installation
------------

You can find current binary packages in [Downloads](https://bitbucket.org/fralik/rundata-qt/downloads/) section. Two versions are provided right now:

* [macOS](https://bitbucket.org/fralik/rundata-qt/downloads/Rundata-qt-v0.0.10.dmg).
* [Windows x64](https://bitbucket.org/fralik/rundata-qt/downloads/Rundata-qt-0.0.10-x64.exe).

The app is still in an early version and binaries are not a priority. One can, however, [build the app from the source code](https://bitbucket.org/fralik/rundata-qt/wiki/Building%20from%20source).

Acknowledgement
---------------

Rundata-qt won't be possible without the help of:

* [Sofia Pereswetoff-Morath](mailto:sofia.pereswetoff-morath@su.se). Sofia provided scientific input and encouragement.
* [Jan Owe](https://runbloggen.gamlebo.se/). Jan is the maintainer of the original Rundata program.

GPLv3 License
-------------

    Copyright (C) 2017 Vadim Frolov

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

Copy of the license can be found in `licenses/LICENSE.GPLv3`.

Rundata-qt depends on Qt framework and KDE Marble. These projects have their own licenses. Corresponding files could be found in `licenses` folder.
