//
// This file is part of Rundata-qt.
//
// This program is free software licensed under the GNU GPL v3 or any later version.
// You can find a copy of this license in LICENSE.GPLv3 in the top directory of
// the source code.
//
// Copyright 2017 Vadim Frolov <fralik@gmail.com>
//
#include "CrossesSqlTable.h"

#include <QImage>
#include <QPixmap>

CrossesSqlTable::CrossesSqlTable(QObject *parent, QSqlDatabase db)
    : QSqlTableModel(parent, db)
{

}

QVariant CrossesSqlTable::data(const QModelIndex &index, int role) const
{
    if (index.column() == 1)
    {
        QString crossName = QSqlTableModel::data(index, Qt::DisplayRole).toString();
        if (role == Qt::DisplayRole)
            return crossName;

        if (crossName.length() > 3)
            return QSqlTableModel::data(index, role);

        QString imgFile(QStringLiteral(":/images/crosses/%1").arg(crossName));
        QPixmap pixmap(imgFile);
        if (role == Qt::DecorationRole)
            return pixmap;
        if (role == Qt::SizeHintRole)
            return pixmap.size();
    }

    return QSqlTableModel::data(index, role);
}
