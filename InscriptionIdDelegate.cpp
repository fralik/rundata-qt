//
// This file is part of Rundata-qt.
//
// This program is free software licensed under the GNU GPL v3 or any later version.
// You can find a copy of this license in LICENSE.GPLv3 in the top directory of
// the source code.
//
// Copyright 2017 Vadim Frolov <fralik@gmail.com>
//
#include "InscriptionIdDelegate.h"
#include <InscriptionTreeModel.h>

#include <QDebug>

InscriptionIdDelegate::InscriptionIdDelegate(QObject *parent)
    : QStyledItemDelegate(parent)
{

}

void InscriptionIdDelegate::paint(QPainter *painter, const QStyleOptionViewItem &option, const QModelIndex &index) const
{
    bool newReadeing = index.data(InscriptionTreeModel::NewReadingRole).toBool();

    QStyleOptionViewItem opt(option);
    opt.font.setBold(newReadeing);

    QStyledItemDelegate::paint(painter, opt, index);
}
