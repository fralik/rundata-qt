//
// This file is part of Rundata-qt.
//
// This program is free software licensed under the GNU GPL v3 or any later version.
// You can find a copy of this license in LICENSE.GPLv3 in the top directory of
// the source code.
//
// Copyright 2017 Vadim Frolov <fralik@gmail.com>
//
#ifndef INSCRIPTION_TREE_ITEM_H
#define INSCRIPTION_TREE_ITEM_H

#include <QList>
#include <QVariant>
#include <QObject>

/**
 * @brief The InscriptionTreeItem class is used to represent an item in a tree model of inscriptions.
 */
class InscriptionTreeItem: public QObject
{

public:
    //Q_PROPERTY(bool lost READ lost)
    //Q_PROPERTY(bool newReading READ newReading)

    explicit InscriptionTreeItem(const QList<QVariant> &data,
                InscriptionTreeItem *parentItem = Q_NULLPTR, QObject *parent = Q_NULLPTR);
    virtual ~InscriptionTreeItem();

    void appendChild(InscriptionTreeItem *child);
    void appendItem(const QVariant& data);

    InscriptionTreeItem *child(int row);
    int childCount() const;
    int columnCount() const;
    QVariant data(int column) const;
    int row() const;
    InscriptionTreeItem *parentItem();
    // Provides column index that contains requested data or -1.
    int columnFromData(const QVariant data) const;

    //bool lost() const;
    //bool newReading() const;

private:
    QList<InscriptionTreeItem*> _childItems;
    QList<QVariant> _itemData; // [signatureID as string; signatureID as int]
    InscriptionTreeItem *_parentItem;
};

#endif // INSCRIPTION_TREE_ITEM_H
