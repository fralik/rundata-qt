#-------------------------------------------------
#
# Project created by QtCreator 2015-06-30T20:22:23
#
#-------------------------------------------------
include(qt_Marble.pri)

QT       += core gui sql widgets Marble

TEMPLATE = app
TARGET = Rundata-qt
VERSION = 0.0.10
DEFINES += RUNDATA_VERSION_STRING=\\\"$$VERSION\\\"

# Disable automatic ASCII conversions (best practice for internationalization).
# Ensures that tr() is used for translatable strings and QLatin1String/QStringLiteral for not-translatable strings
DEFINES += QT_NO_CAST_FROM_ASCII QT_NO_CAST_TO_ASCII

CONFIG += console

SOURCES += main.cpp\
    CrossesSqlTable.cpp \
    FilterItem.cpp \
    PanelWindow.cpp \
    InscriptionProxyModel.cpp \
    FormatSelectorDialog.cpp \
    FormatListProxy.cpp \
    FilterDialog.cpp \
    InscriptionIdDelegate.cpp \
    InscriptionTreeItem.cpp \
    InscriptionTreeModel.cpp \
    TreeViewDraggable.cpp \
    AboutDialog.cpp \
    FilterModel.cpp

HEADERS  += \
    CrossesSqlTable.h \
    FilterItem.h \
    FilterModel.h \
    PanelWindow.h \
    InscriptionProxyModel.h \
    FormatSelectorDialog.h \
    FormatListProxy.h \
    FilterDialog.h \
    InscriptionIdDelegate.h \
    InscriptionTreeItem.h \
    InscriptionTreeModel.h \
    TreeViewDraggable.h \
    AboutDialog.h

FORMS    += \
    PanelWindow.ui \
    FilterDialog.ui \
    FormatSelectorDialog.ui \
    AboutDialog.ui

RESOURCES += myresources.qrc

TRANSLATIONS = translations/rundata_en.ts \
        translations/rundata_sv.ts

# what to copy
licFiles.files = $$quote($$PWD/licenses/LICENSE.GPLv3) \
        $$quote($$PWD/licenses/Marble.LGPLv21) \
        $$quote($$PWD/licenses/Qt.GPLv3)

binaryData.files = $$quote($$PWD/inscriptions.sqlite3) \
                   $$quote($$PWD/inscriptions.kmz) \

win32 {
    MARBLE_LIB_NAME = $$qtLibraryTarget($$QT.Marble.name).$$QMAKE_EXTENSION_SHLIB
    ZLIB_PATTERN = $$QT.Marble.libs/$$QT.Marble.prefix$$qtLibraryTarget("zlib").$$QMAKE_EXTENSION_SHLIB
    ASTRO_PATTERN = $$QT.Marble.libs/$$QT.Marble.prefix$$qtLibraryTarget("astro").$$QMAKE_EXTENSION_SHLIB
} else {
    MARBLE_LIB_NAME = $$QT.Marble.prefix$$qtLibraryTarget($$QT.Marble.name).*$$QMAKE_EXTENSION_SHLIB
    ZLIB_PATTERN = $$QT.Marble.libs/$$QT.Marble.prefix$$qtLibraryTarget("zlib").*$$QMAKE_EXTENSION_SHLIB
    ASTRO_PATTERN = $$QT.Marble.libs/$$QT.Marble.prefix$$qtLibraryTarget("astro").*$$QMAKE_EXTENSION_SHLIB
}
marbleLib.files = $$files($$shell_path($$QT.Marble.libs/$$MARBLE_LIB_NAME))
marbleLib.files += $$files($$clean_path($$ZLIB_PATTERN))
marbleLib.files += $$files($$clean_path($$ASTRO_PATTERN))
marbleLib.files = $$unique(marbleLib.files)
marbleResources.files += $$quote($$QT.Marble.data)
marblePlugins.files += $$quote($$QT.Marble.plugins)
message("Marble libraries: $$marbleLib.files")
message("Marble resources: $$marbleResources.files")

macx {
    # If QtCreator fails to find Marble libraries because there is a file name mismatch, then you have two options:
    # 1. Adjust debug postfix for Marble library in correponding CMakeLists files.
    # 2. Redifine function qtPlatformTargetSuffix and adjust the suffix. This function is found in Qt release
    #    mkspecs/features/qt_functions.prf file. Default Qt function is below:
    #defineReplace(qtPlatformTargetSuffix) {
    #    suffix =
    #    CONFIG(debug, debug|release) {
    #        !debug_and_release|build_pass {
    #            mac: return($${suffix}_debug)
    #            win32: return($${suffix}d)
    #        }
    #    }
    #    return($$suffix)
    #}

    # where to copy
    binaryData.path = Contents/Resources
    licFiles.path = Contents/Resources/licenses
    marbleLib.path = Contents/Frameworks
    marbleResources.path = Contents/Resources
    marblePlugins.path = Contents/PlugIns

    QMAKE_BUNDLE_DATA += binaryData licFiles marbleLib marbleResources marblePlugins
    #QMAKE_INFO_PLIST = Info.plist
    QMAKE_CFLAGS += -gdwarf-2
    QMAKE_CXXFLAGS += -gdwarf-2
    QMAKE_MACOSX_DEPLOYMENT_TARGET = 10.8

    ICON = apple/rundata.icns

    QMAKE_LFLAGS_SONAME  = -Wl,-install_name,@executable_path/../Frameworks/

    prepareBundle.input =
    prepareBundle.commands = $$PWD/prepareMac.sh $$TARGET $$OUT_PWD $$QT.Marble.libs//..
    QMAKE_EXTRA_TARGETS += prepareBundle
}

win32 {
    CONFIG -= debug_and_release embed_manifest_exe
    RC_ICONS = $$quote($$PWD/images/rundata.ico)
    QMAKE_TARGET_COMPANY = "fralik entertainment"
    QMAKE_TARGET_COPYRIGHT = "See Help/About for details"
    QMAKE_TARGET_PRODUCT = "Rundata-qt for Windows"
}

!macx {
    # OUT_PWD can be replaced with an environmnet variable, i.e. installation path
    target.path = $$quote($$OUT_PWD/package)
    binaryData.path = $$quote($$target.path)
    licFiles.path = $$quote($$target.path/licenses)
    marbleLib.path = $$quote($$target.path)
    marbleResources.path = $$quote($$target.path)

    INSTALLS += binaryData licFiles target marbleLib marbleResources
}

## Define installer target
win32 {
    INSTALLER_FILENAME = RundataQt-v$$VERSION-setup

    PKG_DATA = $$PWD/packages/com.vadimfrolov.rundata/data
    MS_PKG_DATA = $$PWD/packages/com.microsoft.vcredist/data
    prepareInstaller.input =
    win32 {
        prepareInstaller.commands += windeployqt --release $$quote($$target.path) --no-system-d3d-compiler -no-angle $$escape_expand(\\n\\t)
        prepareInstaller.commands += windeployqt --release $$quote($$shell_path($$target.path/$$MARBLE_LIB_NAME)) --no-system-d3d-compiler -no-angle --no-translations $$escape_expand(\\n\\t)
        prepareInstaller.commands += $(MOVE) $$shell_quote($$shell_path($$target.path/vcredist_x64.exe)) $$shell_quote($$shell_path($$MS_PKG_DATA/)) $$escape_expand(\\n\\t)
        prepareInstaller.commands += archivegen $$shell_quote($$shell_path($$OUT_PWD/data.7z)) $$shell_quote($$shell_path($$target.path/*)) $$escape_expand(\\n\\t)
        prepareInstaller.commands += $(MOVE) $$shell_quote($$shell_path($$OUT_PWD/data.7z)) $$shell_quote($$shell_path($$PKG_DATA/)) $$escape_expand(\\n\\t)
    }

    QMAKE_EXTRA_TARGETS += prepareInstaller

    INPUT = $$PWD/config/config.xml $$PWD/packages
    offlineInstaller.depends = prepareInstaller
    offlineInstaller.input = INPUT
    offlineInstaller.output = $$INSTALLER_FILENAME
    offlineInstaller.commands = binarycreator -c $$quote($$PWD/config/config.xml) -p $$quote($$PWD/packages) $$INSTALLER_FILENAME
    #offlineInstaller.commands = @echo making installer
    offlineInstaller.CONFIG += target_predeps no_link combine

    QMAKE_EXTRA_TARGETS += offlineInstaller
}
