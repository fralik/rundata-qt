//
// This file is part of Rundata-qt.
//
// This program is free software licensed under the GNU GPL v3 or any later version.
// You can find a copy of this license in LICENSE.GPLv3 in the top directory of
// the source code.
//
// Copyright 2017 Vadim Frolov <fralik@gmail.com>
//
#include "TreeViewDraggable.h"

TreeViewDraggable::TreeViewDraggable(QWidget *parent)
    : QTreeView(parent)
{
    // TODO: implement proper drag'n'drop. See FilterModel::moveRows comments.
    setDragEnabled(false);
    setDropIndicatorShown(true);
    setDragDropMode(QAbstractItemView::InternalMove);
    setDefaultDropAction(Qt::TargetMoveAction);
    setSelectionMode(QAbstractItemView::SingleSelection);
}


void TreeViewDraggable::dropEvent(QDropEvent *event)
{
    QModelIndex droppedIndex = indexAt(event->pos());
    QModelIndex droppedParent = droppedIndex.parent();
    QModelIndex dragIndex = selectionModel()->currentIndex();

    if (!droppedIndex.isValid() || !dragIndex.isValid())
        return;

    if (model() != Q_NULLPTR)
        model()->moveRow(dragIndex.parent(), dragIndex.row(), droppedParent, droppedIndex.row());
}


