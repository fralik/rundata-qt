//
// This file is part of Rundata-qt.
//
// This program is free software licensed under the GNU GPL v3 or any later version.
// You can find a copy of this license in LICENSE.GPLv3 in the top directory of
// the source code.
//
// Copyright 2017 Vadim Frolov <fralik@gmail.com>
//
#ifndef INSCRIPTION_TREE_MODEL_H
#define INSCRIPTION_TREE_MODEL_H

#include <QObject>
#include <QtSql>
#include <QSqlDatabase>
#include <QAbstractItemModel>
#include <QModelIndex>
#include <QVariant>
#include <QStringList>

QT_FORWARD_DECLARE_CLASS(QStandardItemModel)

class QSqlDatabase;
class InscriptionTreeItem;

/**
 * @brief Custom data model that provides information about runic inscriptions.
 *
 * The class copies data from the database and stores it internally in InscriptionTreeItem objects.
 * It also provides additional methods that expose data to user/views.
 */
class InscriptionTreeModel: public QAbstractItemModel
{
    Q_OBJECT

public:
    // additional roles for displayableColumns data model
    enum
    {
        // Roles for model of displayableColumns()
        ColumnNameRole = Qt::UserRole,
        SelectedRole = Qt::UserRole + 1,
        TemporarySelectedRole = Qt::UserRole + 2,
        ShowColumnNameRole = Qt::UserRole + 3,

        // Roles for InscriptionTreeModel:
        IsLostRole = Qt::UserRole,
        NewReadingRole = Qt::UserRole + 1
    };

    explicit InscriptionTreeModel(QSqlDatabase &db, QObject *parent = 0);
    ~InscriptionTreeModel();

    // QAbstractItemModel interface
    QVariant data(const QModelIndex &index, int role = Qt::DisplayRole) const Q_DECL_OVERRIDE;
    Qt::ItemFlags flags(const QModelIndex &index) const Q_DECL_OVERRIDE;
    QModelIndex index(int row, int column, const QModelIndex &parent = QModelIndex()) const Q_DECL_OVERRIDE;
    QModelIndex parent(const QModelIndex &child) const Q_DECL_OVERRIDE;
    int rowCount(const QModelIndex &parent = QModelIndex()) const Q_DECL_OVERRIDE;
    int columnCount(const QModelIndex &parent = QModelIndex()) const Q_DECL_OVERRIDE;
    QVariant headerData(int section, Qt::Orientation orientation, int role = Qt::DisplayRole) const Q_DECL_OVERRIDE;

    // Return list of human-readable names of searchable field.
    // A searchable field is a single element/parameter for search.
    // For example, 'Found location'.
    QMap<QString, QString> searchableColumns() const;

    // QStandardItemModel that represents data columns that are shown to user.
    // This can be an independent class, but since our data source is almost static,
    // it is a bit easier to keep it inside InscriptionTreeModel.
    QAbstractItemModel *displayableColumns() const;

    // Return an item model of values existing in DB for a given search column.
    // columnName should be one of searchableFields.
    QAbstractItemModel *uniqueValues(const QString& columnName) const;

    // Return list of valid signature IDs based on query sqlQuery.
    QList<int> signaturesForQuery(const QString sqlQuery);

    int columnIndexByName(const QString& columnName) const;

    // Return crosses of a signature specified by signature model index.
    // Result is a 3D array. 1st dimension corresponds to number of crosses.
    // 2nd dimension contains up to 7 elements (one per each cross style group).
    // 3rd dimension specifies elements in each of a style group
    // A signature can have multiple crosses (1st dim), where each cross is described by
    // several style groups (2nd dim). And a cross can have multiple entries (3rd dim)
    // within each style group.
    QList<QVector<QStringList> > crosses(const QModelIndex &signatureIndex) const;

    // Debug method. TOODO: remove it!
    static void dumpModel(QAbstractItemModel *model, const QString &additional);

private:
    InscriptionTreeItem *getItem(const QModelIndex &index) const;
    bool isLost(InscriptionTreeItem *item) const;
    bool hasNewReading(InscriptionTreeItem *item) const;

    void setupModelData();
    // We need this to initialize the list in specific order, addToDisplayableColumns is a more general function
    void initDisplayableColumns(const QStringList &dbNames, const QStringList &humanNames);
    void addToDisplayableColumns(const QMap<QString, QString> &map);

    QSqlDatabase *_db;
    InscriptionTreeItem *_rootItem;

    // Key is a DB column name, value - human-readable version of that column name
    // Values of this map can also be seen as header for the data model
    QMap<QString, QString> _searchColumns;
    // See comments for displayableColumns(). The model has one column, but each item
    // provides sub-items through roles. See the enum above.
    QStandardItemModel *_displayableColumns;

    // Query model to get values from the DB as an item model
    QSqlQueryModel *_sqlModel;

    // Column index that contains 'lost'flag. -1 if not present.
    int _colLost;
    // Column index that contains 'new reading'flag. -1 if not present.
    int _colNewReading;
};

#endif // INSCRIPTION_TREE_MODEL_H
