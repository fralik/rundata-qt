//
// This file is part of Rundata-qt.
//
// This program is free software licensed under the GNU GPL v3 or any later version.
// You can find a copy of this license in LICENSE.GPLv3 in the top directory of
// the source code.
//
// Copyright 2017 Vadim Frolov <fralik@gmail.com>
//
#ifndef FILTERMODEL_H
#define FILTERMODEL_H

#include <QAbstractItemModel>
#include <QModelIndex>
#include <QVariant>

class FilterItem;

/**
 * @brief Model that stores inscription search parameters.
 *
 * FilterModel organizes individual FilterItem items into a tree
 * structure.
 * FilterModel is used during the search in inscriptions.
 */
class FilterModel : public QAbstractItemModel
{
    Q_OBJECT

public:
    explicit FilterModel(const QStringList &headers, const QString& data, QObject *parent = Q_NULLPTR);
    ~FilterModel();

    QVariant data(const QModelIndex &index, int role) const Q_DECL_OVERRIDE;
    QVariant headerData(int section, Qt::Orientation orientation,
                        int role = Qt::DisplayRole) const Q_DECL_OVERRIDE;

    QModelIndex index(int row, int column, const QModelIndex &parent = QModelIndex()) const Q_DECL_OVERRIDE;
    QModelIndex parent(const QModelIndex &index) const Q_DECL_OVERRIDE;
    int rowCount(const QModelIndex &parent = QModelIndex()) const Q_DECL_OVERRIDE;
    int columnCount(const QModelIndex &parent = QModelIndex()) const Q_DECL_OVERRIDE;

    Qt::ItemFlags flags(const QModelIndex &index) const Q_DECL_OVERRIDE;
    bool setData(const QModelIndex &index, const QVariant &value, int role = Qt::EditRole) Q_DECL_OVERRIDE;
    bool setHeaderData(int section, Qt::Orientation orientation, const QVariant &value, int role = Qt::EditRole) Q_DECL_OVERRIDE;
    bool insertRows(int row, int count, const QModelIndex &parent = QModelIndex()) Q_DECL_OVERRIDE;
    bool insertColumns(int column, int count, const QModelIndex &parent = QModelIndex()) Q_DECL_OVERRIDE;
    bool removeRows(int row, int count, const QModelIndex &parent = QModelIndex()) Q_DECL_OVERRIDE;
    bool removeColumns(int column, int count, const QModelIndex &parent = QModelIndex()) Q_DECL_OVERRIDE;
    bool moveRows(const QModelIndex &sourceParent, int sourceRow, int count, const QModelIndex &destinationParent, int destinationChild) Q_DECL_OVERRIDE;

    Qt::DropActions supportedDropActions() const Q_DECL_OVERRIDE;
    Qt::DropActions supportedDragActions() const Q_DECL_OVERRIDE;

    // Makes model representation in a string. Suitable for storing in a file.
    /**
     * @brief Returns current model in a string.
     * @details Each individual item is separated by a new line. Children are offset by
     * a single whitespace.
     */
    QString toString() const;

    /**
     * @brief Initialize model with data from a s tring.
     * @param str Result of FilterModel::toString.
     */
    void fromString(const QString& str);

private:
    FilterItem *_getItem(const QModelIndex &index) const;
    FilterItem *_rootItem;
};

#endif // FILTERMODEL_H
