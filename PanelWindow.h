//
// This file is part of Rundata-qt.
//
// This program is free software licensed under the GNU GPL v3 or any later version.
// You can find a copy of this license in LICENSE.GPLv3 in the top directory of
// the source code.
//
// Copyright 2017 Vadim Frolov <fralik@gmail.com>
//
#ifndef PANELWINDOW_H
#define PANELWINDOW_H

#include <QMainWindow>
#include <QtSql>
#include <QList>
class QMenu;

namespace Ui {
class PanelWindow;
}

class FilterModel;
class FilterDialog;
class DbController;
class InscriptionProxyModel;
class InscriptionTreeModel;
namespace Marble {
    class GeoDataDocument;
}
QT_FORWARD_DECLARE_CLASS(QTextDocument)

class PanelWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit PanelWindow(QWidget *parent = 0);
    ~PanelWindow();

private slots:
    void onSignaturesSelectionChanged(const QItemSelection&, const QItemSelection &);
    void onCustomContextMenu(const QPoint &pt);
    // Take action when list of inscriptions has been changed. This can happen when filter is applied.
    void onSignaturesListChanged();

    void onFilterMenuAboutToShow();
    void on_actionAddNewParameter_triggered();
    void on_actionAddSubparameter_triggered();
    void on_actionEditParameter_triggered();
    void on_actionRemoveParameter_triggered();
    void on_actionClearParameters_triggered();
    void on_actionExportParameters_triggered();
    void on_actionImportParameters_triggered();

    void on_actionReload_style_triggered();
    void on_actionShowMap_triggered(bool checked);

    void on_actionAbout_triggered();

    void onFilterModelChange();
    void onFilterStateToggle(bool);
    void onFilterViewDoubleClick(const QModelIndex &index);
    void onFilterSelectionChanged(const QItemSelection &selected, const QItemSelection &deselected);
    void onCallTextFormat(bool);
    // Save current state including user data
    void saveState();

protected:
    void closeEvent(QCloseEvent *event) Q_DECL_OVERRIDE;

private:
    void displaySignatureInfo();
    void restoreDisplayColumns();
    void restoreAppearance();
    void createKml();
    void initDefaultState();
    void initValidState();

    Ui::PanelWindow *ui;
    QSqlTableModel *_signaturesModel;
    QSqlDatabase _db;
    InscriptionProxyModel *_signaturesListProxy; // panel 1, list of available signatures
    FilterModel *_filterModel;
    FilterDialog *_searchDialog;
    InscriptionTreeModel *_allData;
    Marble::GeoDataDocument *_placemarks;
    QTextDocument* _displayDocument;
    QHash<QString, int> _signature2placemark;
    bool _isClosing;
    QString _lastExportFilename;
};

#endif // PANELWINDOW_H
