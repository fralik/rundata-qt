//
// This file is part of Rundata-qt.
//
// This program is free software licensed under the GNU GPL v3 or any later version.
// You can find a copy of this license in LICENSE.GPLv3 in the top directory of
// the source code.
//
// Copyright 2017 Vadim Frolov <fralik@gmail.com>
//
#ifndef FILTER_DIALOG_H
#define FILTER_DIALOG_H

#include <QDialog>
#include <QVariant>
#include <QAbstractTableModel>

QT_FORWARD_DECLARE_CLASS(QStandardItemModel)

namespace Ui {
class FilterDialog;
}

class InscriptionTreeModel;

/**
 * @brief Dialog to enter search values for different data columns.
 *
 * User can query different pieces of information, which is represented
 * column-wise. This dialog allows user to select a column and enter
 * a search value. In addition, user is presented with a choice of logical
 * operators that define search conditions.
 */
class FilterDialog : public QDialog
{
    Q_OBJECT

public:
    explicit FilterDialog(const InscriptionTreeModel *model, QWidget *parent = Q_NULLPTR);
    ~FilterDialog();

    void setSearchField(const QString &fieldName);
    void setSearchValue(const QString &value);
    void setCondition(const QString &logicalOperator);
    /**
     * @brief Initialize model in order to show information about crosses
     * @param model A model with different columns one of which contains crosses
     * @param modelColumn Column in the model that contains crosses
     */
    void setCrossesModel(QAbstractTableModel *model, int modelColumn);
    /**
     * @brief Set availability of conditioning controls through @p enabled.
     * Logical condition is not used for boolean values. This method allows
     * to enable or disable logical condition controls when neeeded.
     */
    void setConditionEnabled(bool enabled);

    /**
     * @brief Return search condition as a string
     */
    QString condition() const;
    QString searchField() const;
    QVariant searchValue() const;
    QString columnName() const;

public slots:
    void reset();

protected:
    bool eventFilter(QObject *object, QEvent *event) Q_DECL_OVERRIDE;

private slots:
    void onFieldChanged(int index);

private:
    Ui::FilterDialog *ui;
    QAbstractTableModel * _crossesModel;
    int _crossesModelColumn;
    const InscriptionTreeModel& _model;
    QStandardItemModel *_yesNoModel;
};

#endif // FILTER_DIALOG_H
