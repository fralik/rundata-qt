#!/bin/bash

# This script is used to prepare a binary bundle on macOS. It is still work in progress.
# It needs to find out Marble version dynamically. It also need to find Qt bin directory dynamically.
# The script work only for release build!

shopt -s extglob

if [ $# -lt 3 ]; then
	echo -e "Run me as:\nprepareMac.sh <targetName> <appBundlePath> <marblePath>";
	echo " <targetName>       name of the target without .app. For example Rundata-qt'";
	echo " <appBundlePath>    path to the folder where app bundle is located. (without /)";
	echo " <marblePath>       path to Marble installation folder (without /).";
	exit 1;
fi
APP_NAME="$1"
APP_DIR="$2/$APP_NAME.app"
#MRBL_DIR="/Users/vadim/marble/install-custom-release"
MRBL_DIR="$3"

BASE_DIR="$APP_DIR/Contents"
PLG_DIR="$BASE_DIR/PlugIns/plugins"
CUR_DIR=$(pwd)

# Get absolute path of this script (https://stackoverflow.com/questions/4774054/reliable-way-for-a-bash-script-to-get-the-full-path-to-itself)
pushd `dirname $0` > /dev/null
SCRIPT_PATH=`pwd -P`
popd > /dev/null

rm -rf $BASE_DIR/resources/data/maps/moon
# Remove all maps, but leave wikimedia
rm -rf $BASE_DIR/resources/data/maps/earth/!(wikimedia)

cp -R $SCRIPT_PATH/wikimedia $BASE_DIR/Resources/data/maps/earth/
cp -R $MRBL_DIR/lib/lib* $BASE_DIR/Frameworks/
rm $BASE_DIR/Frameworks/libmarbledeclarative.dylib
rm -rf $BASE_DIR/Resources/data/audio
rm -rf $BASE_DIR/Resources/data/bitmaps/stars
rm -rf $BASE_DIR/Resources/data/flags
rm -rf $BASE_DIR/Resources/data/licenses
rm -rf $BASE_DIR/Resources/data/stars
rm -rf $BASE_DIR/Resources/data/svg/!(osmcart|worldmap.svg)
rm -rf $BASE_DIR/Resources/data/weather

## remove some plugins
PLUGINS_TO_DELETE=(libAtmospherePlugin.so libAprsPlugin.so libCompassFloatItem.so libCycleStreetsPlugin.so libEarthquakePlugin.so
libEclipsesPlugin.so
libElevationProfileFloatItem.so
libElevationProfileMarker.so
libFlightGearPositionProviderPlugin.so
libFoursquarePlugin.so
libGosmoreReverseGeocodingPlugin.so
libGosmoreRoutingPlugin.so
libGpsbabelPlugin.so
libGpsInfo.so
libGpxPlugin.so
libHostipPlugin.so
libJsonPlugin.so
libMeasureTool.so
libMonavPlugin.so
libNominatimReverseGeocodingPlugin.so
libNominatimSearchPlugin.so
libNotesPlugin.so
libOpenLocationCodeSearchPlugin.so
libOpenRouteServicePlugin.so
libOSRMPlugin.so
libPostalCode.so
libProgressFloatItem.so
libQtPositioningPositionProviderPlugin.so
libRoutingPlugin.so
libRoutinoPlugin.so
libSatellitesPlugin.so
libSpeedometer.so
libStarsPlugin.so
libSunPlugin.so
libYoursPlugin.so)

for i in "${PLUGINS_TO_DELETE[@]}"; do
	rm -rf "${PLG_DIR}/$i"
done

install_name_tool -change @executable_path/lib/libmarblewidget-qt5.28.dylib @rpath/libmarblewidget-qt5.28.dylib $BASE_DIR/MacOS/$APP_NAME 
install_name_tool -change @executable_path/lib/libastro.1.dylib @rpath/libastro.1.dylib $BASE_DIR/Frameworks/libmarblewidget-qt5.0.28.0.dylib 
install_name_tool -delete_rpath /Users/vadim/marble/install-release/lib $BASE_DIR/Frameworks/libmarblewidget-qt5.0.28.0.dylib
install_name_tool -delete_rpath /Users/vadim/marble/install-release/lib $BASE_DIR/Frameworks/libastro.0.17.20.dylib

install_name_tool -delete_rpath /Users/vadim/Qt/5.7/clang_64/lib $BASE_DIR/Frameworks/libmarblewidget-qt5.0.28.0.dylib
#install_name_tool -delete_rpath /Users/vadim/Qt/5.7/clang_64/lib $BASE_DIR/Frameworks/libastro.0.17.20.dylib

install_name_tool -add_rpath @executable_path/../Frameworks $BASE_DIR/Frameworks/libmarblewidget-qt5.0.28.0.dylib
install_name_tool -add_rpath @executable_path/../Frameworks $BASE_DIR/Frameworks/libastro.0.17.20.dylib

install_name_tool -id @executable_path/../Frameworks/libmarblewidget-qt5.0.28.0.dylib $BASE_DIR/Frameworks/libmarblewidget-qt5.0.28.0.dylib
install_name_tool -id @executable_path/../Frameworks/libastro.0.17.20.dylib $BASE_DIR/Frameworks/libastro.0.17.20.dylib


cd $PLG_DIR
for f in *.so; do
	install_name_tool -change @executable_path/lib/libmarblewidget-qt5.28.dylib @rpath/libmarblewidget-qt5.28.dylib $f
	install_name_tool -change @executable_path/lib/libastro.1.dylib @rpath/libastro.1.dylib $f
done
cd $CUR_DIR