//
// This file is part of Rundata-qt.
//
// This program is free software licensed under the GNU GPL v3 or any later version.
// You can find a copy of this license in LICENSE.GPLv3 in the top directory of
// the source code.
//
// Copyright 2017 Vadim Frolov <fralik@gmail.com>
//
#ifndef INSCRIPTIONPROXYMODEL_H
#define INSCRIPTIONPROXYMODEL_H

#include <QSortFilterProxyModel>

/**
 * @brief The InscriptionProxyModel class allows to show only some inscriptions.
 *
 * The class takes a list of valid insctiption indices and proxies those.
 */
class InscriptionProxyModel : public QSortFilterProxyModel
{
    Q_OBJECT

public:
    explicit InscriptionProxyModel(QObject *parent = Q_NULLPTR);

    void setValidIndices(const QList<int> indices);

    void setEnabled(bool enabled);
    bool isEnabled() const { return _enabled; }

Q_SIGNALS:
    /**
     * @brief Signal is emited whenever filter is changed or activated.
     */
    void filterChanged();

protected:
    bool filterAcceptsRow(int source_row, const QModelIndex &source_parent) const Q_DECL_OVERRIDE;

private:
    QList<int> _validIds;
    bool _enabled;
};

#endif // INSCRIPTIONPROXYMODEL_H
