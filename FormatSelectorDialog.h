//
// This file is part of Rundata-qt.
//
// This program is free software licensed under the GNU GPL v3 or any later version.
// You can find a copy of this license in LICENSE.GPLv3 in the top directory of
// the source code.
//
// Copyright 2017 Vadim Frolov <fralik@gmail.com>
//
#ifndef FORMATSELECTORDIALOG_H
#define FORMATSELECTORDIALOG_H

#include <QDialog>
#include <QItemSelection>

namespace Ui {
class FormatSelectorDialog;
}

QT_FORWARD_DECLARE_CLASS(QAbstractItemModel)
QT_FORWARD_DECLARE_CLASS(QSortFilterProxyModel)
QT_FORWARD_DECLARE_CLASS(QListWidgetItem)
class FormatListProxy;

/**
 * @brief Selector of inscription description fields
 *
 * This is a dialog that allows user to select what information about
 * insription is displayed in the main window.
 */
class FormatSelectorDialog : public QDialog
{
    Q_OBJECT

public:
    explicit FormatSelectorDialog(QWidget *parent = Q_NULLPTR);
    ~FormatSelectorDialog();
    /**
     * @brief @p newModel shall contain list of items that represent information fields.
     * @details The model is assumed to be SignatureTreeModel::displayableColumns. The model
     * should have a data role SignatureTreeModel::TemporarySelectedRole,
     * which can cast to bool.
     */
    void setModel(QAbstractItemModel *newModel);
    bool displayHeaders() const;

private slots:
    void onAvailableDoubleClick(const QModelIndex &index);
    void onSelectedDoubleClick(const QModelIndex &index);
    void onAddButtonClick();
    void onAddAllButtonClick();
    void onRemoveButtonClick();
    void onRemoveAllButtonClick();
    void onAccepted();
    void onRejected();
    void onMoveUpClick();
    void onMoveDownClick();
    void onSelectedSelectionChanged(const QItemSelection &selected, const QItemSelection &deselected);
    void onAvailableSelectionChanged(const QItemSelection &selected, const QItemSelection &deselected);

private:
    void resetSelector();
    void checkButtonsState();
    /**
     * @brief Move selected item one position up or down based on @p offset.
     * @param offset 1 moves item down, -1 moves item up.
     */
    void moveSelectedItem(int offset) const;

    Ui::FormatSelectorDialog *ui;
    // Model that is used to populate proxies and contains all possible information fields.
    QAbstractItemModel *_model;
    // A proxy that contains list of available infromation fields. These are the fields,
    // that have SignatureTreeModel::TemporarySelectedRole == false.
    FormatListProxy *_availableProxy;
    // A proxy that contains list of infromation fields selected by user. These are the fields,
    // that have SignatureTreeModel::TemporarySelectedRole == true.
    FormatListProxy *_selectedProxy;
};

#endif // FORMATSELECTORDIALOG_H
