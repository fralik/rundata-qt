//
// This file is part of Rundata-qt.
//
// This program is free software licensed under the GNU GPL v3 or any later version.
// You can find a copy of this license in LICENSE.GPLv3 in the top directory of
// the source code.
//
// Copyright 2017 Vadim Frolov <fralik@gmail.com>
//
#ifndef FORMAT_LIST_PROXY_H
#define FORMAT_LIST_PROXY_H

#include <QSortFilterProxyModel>

/**
 * @brief The FormatListProxy class aids view into SignatureTreeModel::displayableColumns.
 *
 * The proxy is used to sort and filter model items. Filtering happens based on condition, which
 * is set in setProxyCondition. A row is visible is condition matches model item's value
 * under SignatureTreeModel::TemporarySelectedRole role.
 * In addition, the proxy enables drag and drop behaviour.
 */
class FormatListProxy : public QSortFilterProxyModel
{
    Q_OBJECT

public:
    explicit FormatListProxy(QObject *parent = 0);
    void setProxyCondition(bool condition) { _necessaryCondition = condition; }
    bool proxyCondition() const { return _necessaryCondition; }
    bool dropMimeData(const QMimeData *data, Qt::DropAction action, int row, int column, const QModelIndex &parent) Q_DECL_OVERRIDE;
    //bool canDropMimeData(const QMimeData *data, Qt::DropAction action, int row, int column, const QModelIndex &parent) const;

protected:
    bool filterAcceptsRow(int source_row, const QModelIndex &source_parent) const Q_DECL_OVERRIDE;

private:
    bool _necessaryCondition;
};

#endif // FORMAT_LIST_PROXY_H
