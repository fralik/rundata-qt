function Component()
{
}

Component.prototype.createOperations = function()
{
    try {
        // call the base create operations function
        component.createOperations();

        if (systemInfo.productType === "windows") {
            console.log("We are about to create a shortcut");
            component.addOperation("CreateShortcut", "@TargetDir@/Rundata-qt-git.exe", "@StartMenuDir@/Rundata-Qt.lnk",
                "workingDirectory=@TargetDir@", "iconPath=@TargetDir@/Rundata-qt-git.exe");
            component.addOperation("CreateShortcut", "@TargetDir@/maintenancetool.exe", "@StartMenuDir@/Uninstall Rundata-Qt.lnk",
                "workingDirectory=@TargetDir@", "iconPath=@TargetDir@/Rundata-qt-git.exe");
        }
    } catch (e) {
        console.log(e);
    }
}
