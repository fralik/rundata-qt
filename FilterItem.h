//
// This file is part of Rundata-qt.
//
// This program is free software licensed under the GNU GPL v3 or any later version.
// You can find a copy of this license in LICENSE.GPLv3 in the top directory of
// the source code.
//
// Copyright 2017 Vadim Frolov <fralik@gmail.com>
//
#ifndef FILTERITEM_H
#define FILTERITEM_H

#include <QList>
#include <QVariant>
#include <QVector>


/**
 * @brief The FilterItem class represents a single item for inscription filter.
 *
 * Inscription filter is used to search in inscriptions. FilterItem class
 * contains a single search parameter.
 * This class is also used as an item in filters tree view.
 * Each FilterItem consists of a vector with item's data and a list of children.
 * Each child is a FilterItem by itself. One can view top-level of a FilterItem
 * as a table: rows contain children of a current item, columns contain data of
 * a current item. The whole picture is that each row points to another table because
 * each row is a FilterItem.
 */
class FilterItem
{
public:
    explicit FilterItem(const QVector<QVariant> &data, FilterItem *parent = Q_NULLPTR);
    ~FilterItem();

    // Returns a child FilterItem given by row
    FilterItem *child(int row);
    // Returns number of children
    int childCount() const;
    // Returns number of data items in current FilterItem
    int columnCount() const;

    // Returns item's data given by specific column
    QVariant data(int column) const;
    bool setData(int column, const QVariant& value);

    /** Insert \arg count number of children each with \arg columns empty
     * data items starting from \arg position.
     **/
    bool insertChildren(int position, int count, int columns);
    /** Add \arg columns count of columns to data items starting at \arg position.
     **/
    bool insertColumns(int position, int columns);
    FilterItem *parent() const;

    bool removeChildren(int position, int count);
    bool removeColumns(int position, int columns);

    // Swap children and change their order of appearance
    bool swapChildren(const int source, const int destination);

    int row() const;

    /**
     * @brief Returns current item values as a tab-delimited string
     */
    QString toString() const;

private:
    QList<FilterItem*> _childItems;
    QVector<QVariant> _itemData;
    FilterItem* _parent;
};

#endif // FILTERITEM_H
