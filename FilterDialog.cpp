//
// This file is part of Rundata-qt.
//
// This program is free software licensed under the GNU GPL v3 or any later version.
// You can find a copy of this license in LICENSE.GPLv3 in the top directory of
// the source code.
//
// Copyright 2017 Vadim Frolov <fralik@gmail.com>
//
#include "FilterDialog.h"
#include "ui_filterdialog.h"
#include "InscriptionTreeModel.h"

#include <QSqlTableModel>
#include <QAbstractItemModel>
#include <QMap>
#include <QSqlQueryModel>
#include <QStandardItem>
#include <QLatin1String>
#include <QKeyEvent>
#include <QCompleter>

FilterDialog::FilterDialog(const InscriptionTreeModel *model, QWidget *parent) :
    QDialog(parent),
    ui(new Ui::FilterDialog)
  , _crossesModel(Q_NULLPTR)
  , _crossesModelColumn(-1)
  , _model(*model)
{
    ui->setupUi(this);
    setModal(true);

    auto proxyModel = new QSortFilterProxyModel;
    proxyModel->setSourceModel(model->displayableColumns());
    ui->fieldSelector->setModel(proxyModel);

    proxyModel->setFilterKeyColumn(0);
    proxyModel->setSortCaseSensitivity(Qt::CaseInsensitive);
    proxyModel->sort(0);

    _yesNoModel = new QStandardItemModel(this);
    _yesNoModel->appendRow(new QStandardItem(QStringLiteral("yes")));
    _yesNoModel->appendRow(new QStandardItem(QStringLiteral("no")));

    connect(ui->fieldSelector, SIGNAL(currentIndexChanged(int)), this, SLOT(onFieldChanged(int)));
    ui->fieldSelector->setCurrentIndex(1);

    ui->comboBox->installEventFilter(this);
    ui->comboBox->completer()->setCompletionMode(QCompleter::PopupCompletion);
    ui->comboBox->completer()->setModelSorting(QCompleter::CaseInsensitivelySortedModel);
}

FilterDialog::~FilterDialog()
{
    delete ui;
}

void FilterDialog::setSearchField(const QString &fieldName)
{
    ui->fieldSelector->setCurrentText(fieldName);
}

void FilterDialog::setSearchValue(const QString &value)
{
    ui->comboBox->setCurrentText(value);
}

void FilterDialog::setCondition(const QString &logicalOperator)
{
    // Logical operator can be, for example, AND or AND NOT
    int firstSpace = logicalOperator.indexOf(QChar::Space, 0);
    QString operatorValue = logicalOperator;
    bool exclusionFlag = false;

    if (firstSpace != -1)
    {
        operatorValue = logicalOperator.left(firstSpace);
        exclusionFlag = true;
    }

    if (operatorValue.compare(QLatin1String("and"), Qt::CaseInsensitive) == 0)
    {
        ui->btnAdd->setChecked(true);
    }
    else if (operatorValue.compare(QLatin1String("or"), Qt::CaseInsensitive) == 0)
    {
        ui->btnOr->setChecked(true);
    }

    ui->btnNot->setChecked(exclusionFlag);
}

void FilterDialog::setCrossesModel(QAbstractTableModel *model, int modelColumn)
{
    if (model == _crossesModel || model == Q_NULLPTR)
        return;
    _crossesModel = model;
    _crossesModelColumn = modelColumn;
}

void FilterDialog::setConditionEnabled(bool enabled)
{
    ui->btnNot->setEnabled(enabled);
    ui->btnAdd->setEnabled(enabled);
    ui->btnOr->setEnabled(enabled);
}

QString FilterDialog::condition() const
{
    if (ui->btnNot->isChecked())
        return QStringLiteral("%1 NOT").arg(ui->buttonGroup->checkedButton()->text());
    else
        return ui->buttonGroup->checkedButton()->text();
}

QString FilterDialog::searchField() const
{
    return ui->fieldSelector->currentText();
}

QVariant FilterDialog::searchValue() const
{
    return ui->comboBox->currentText();
}

QString FilterDialog::columnName() const
{
    return ui->fieldSelector->currentData().toString();
}

void FilterDialog::reset()
{
    ui->fieldSelector->setCurrentIndex(1);
    ui->comboBox->setCurrentText(QString());
    ui->btnAdd->setEnabled(true);
    ui->btnNot->setChecked(false);
}

// This filter is needed to detect Enter key in ComboBox. If it is
// not interrupted, then Enter deletes current text.
bool FilterDialog::eventFilter(QObject *object, QEvent *event)
{
    if (event->type() == QEvent::KeyPress)
    {
        QKeyEvent *keyEvent = static_cast<QKeyEvent*>(event);
        if (keyEvent->key() == Qt::Key_Enter || keyEvent->key() == Qt::Key_Return)
        {
            accept();
            return true;
        }
    }
    return QDialog::eventFilter(object, event);
}

void FilterDialog::onFieldChanged(int index)
{
    Q_UNUSED(index);

    if (ui->fieldSelector->currentData().toString() == QLatin1String("crosses"))
    {
        // show crosses selection dialog
        ui->comboBox->setModel(_crossesModel);
        ui->comboBox->setModelColumn(_crossesModelColumn);
        setConditionEnabled(true);
        return;
    }
    if (ui->fieldSelector->currentData().toString() == QLatin1String("hasAlternatives")
            || ui->fieldSelector->currentData().toString() == QLatin1String("all_data.lost")
            || ui->fieldSelector->currentData().toString() == QLatin1String("all_data.new_reading"))
    {
        ui->comboBox->setModel(_yesNoModel);
        // TODO: refactor setConditionEnabled, so that the following 3 lines can be changed
        ui->btnAdd->setEnabled(true);
        ui->btnOr->setEnabled(true);
        ui->btnNot->setEnabled(false);
        return;
    }
    ui->comboBox->setModel(_model.uniqueValues(columnName()));
    ui->comboBox->setModelColumn(0);
    setConditionEnabled(true);
}
