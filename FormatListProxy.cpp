//
// This file is part of Rundata-qt.
//
// This program is free software licensed under the GNU GPL v3 or any later version.
// You can find a copy of this license in LICENSE.GPLv3 in the top directory of
// the source code.
//
// Copyright 2017 Vadim Frolov <fralik@gmail.com>
//
#include "FormatListProxy.h"
#include "InscriptionTreeModel.h"

#include <QDebug>

FormatListProxy::FormatListProxy(QObject *parent)
    : QSortFilterProxyModel(parent)
    , _necessaryCondition(false)
{

}

bool FormatListProxy::filterAcceptsRow(int source_row, const QModelIndex &source_parent) const
{
    QModelIndex index = sourceModel()->index(source_row, 0, source_parent);
    bool selected = sourceModel()->data(index, InscriptionTreeModel::TemporarySelectedRole).toBool();
    return selected == _necessaryCondition;
}


bool FormatListProxy::dropMimeData(const QMimeData *data, Qt::DropAction action,
                                       int row, int column, const QModelIndex &parent)
{
    /**
     * This only allows to move entries along one view. To be able to drag and drop items
     * between views, I have to subclass QStandardItemModel and probably do smth when
     * drag starts.
     */

    if (parent.isValid() && parent != QModelIndex())
    {
        // this is a child, we have to adjust it
        row = parent.row();
        column = parent.column();
    }
    bool base = QSortFilterProxyModel::dropMimeData(data, action, row, column, QModelIndex());
    return base;
}
