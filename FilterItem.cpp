//
// This file is part of Rundata-qt.
//
// This program is free software licensed under the GNU GPL v3 or any later version.
// You can find a copy of this license in LICENSE.GPLv3 in the top directory of
// the source code.
//
// Copyright 2017 Vadim Frolov <fralik@gmail.com>
//
#include "FilterItem.h"

FilterItem::FilterItem(const QVector<QVariant> &data, FilterItem *parent)
{
    _parent = parent;
    _itemData = data;
}

FilterItem::~FilterItem()
{
    qDeleteAll(_childItems);
}

FilterItem *FilterItem::child(int row)
{
    return _childItems.value(row);
}

int FilterItem::childCount() const
{
    return _childItems.count();
}

int FilterItem::columnCount() const
{
    return _itemData.count();
}

QVariant FilterItem::data(int column) const
{
    return _itemData.value(column);
}

bool FilterItem::setData(int column, const QVariant &value)
{
    if (column < 0 || column >= _itemData.size())
        return false;

    _itemData[column] = value;
    return true;
}

bool FilterItem::insertChildren(int position, int count, int columns)
{
    if (position < 0 || position > _childItems.size())
        return false;

    for (int row = 0; row < count; ++row)
    {
        QVector<QVariant> data(columns);
        auto item = new FilterItem(data, this);
        _childItems.insert(position, item);
    }

    return true;
}

bool FilterItem::insertColumns(int position, int columns)
{
    if (position < 0 || position > _itemData.size())
        return false;

    for (int column = 0; column < columns; ++column)
        _itemData.insert(position, QVariant());

    foreach (auto child, _childItems)
        child->insertColumns(position, columns);

    return true;
}

// A.K.A. childNumber
int FilterItem::row() const
{
    if (_parent)
        return _parent->_childItems.indexOf(const_cast<FilterItem*>(this));

    return 0;
}

QString FilterItem::toString() const
{
    QStringList items;
    for (int i = 0; i < columnCount(); ++i)
        items << this->data(i).toString();

    return items.join(QLatin1Char('\t'));
}

FilterItem *FilterItem::parent() const
{
    return _parent;
}

bool FilterItem::removeChildren(int position, int count)
{
    if (position < 0 || position + count > _childItems.size())
        return false;

    for (int row = 0; row < count; ++row)
        delete _childItems.takeAt(position);

    return true;
}

bool FilterItem::removeColumns(int position, int columns)
{
    if (position < 0 || position + columns > _itemData.size())
        return false;

    for (int column = 0; column < columns; ++column)
        _itemData.remove(position);

    foreach (auto child, _childItems)
        child->removeColumns(position, columns);

    return true;
}

bool FilterItem::swapChildren(const int source, const int destination)
{
    if (source < 0 || source > _childItems.count()
            || destination < 0 || destination > _childItems.count())
    {
        return false;
    }

    _childItems.swap(source, destination);
    return true;
}

