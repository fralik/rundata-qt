//
// This file is part of Rundata-qt.
//
// This program is free software licensed under the GNU GPL v3 or any later version.
// You can find a copy of this license in LICENSE.GPLv3 in the top directory of
// the source code.
//
// Copyright 2017 Vadim Frolov <fralik@gmail.com>
//
#include "PanelWindow.h"
#include "ui_PanelWindow.h"
#include "FilterDialog.h"
#include "FilterModel.h"
#include "InscriptionProxyModel.h"
#include "FormatSelectorDialog.h"
#include "InscriptionTreeModel.h"
#include "InscriptionIdDelegate.h"
#include "CrossesSqlTable.h"
#include "AboutDialog.h"

#include "marble/MarbleWidget.h"
#include "marble/GeoDataDocument.h"
#include "marble/GeoDataPlacemark.h"
#include "marble/GeoDataTreeModel.h"
#include "marble/MarbleModel.h"
#include "marble/GeoDataBalloonStyle.h"
#include "marble/GeoDataIconStyle.h"
#include "marble/AbstractFloatItem.h"
#include "marble/GeoWriter.h"
#include "marble/ParsingRunnerManager.h"

#include <QDebug>
#include <QCompleter>
#include <QTreeWidget>
#include <QMenu>
#include <QAction>
#include <QFileDialog>
#include <QImageReader>
#include <QTextStream>
#include <QStandardItemModel>
#include <QDesktopWidget>
#include <QRect>
#include <QSaveFile>
#include <QMessageBox>

using namespace Marble;

PanelWindow::PanelWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::PanelWindow)
    , _signaturesModel(Q_NULLPTR)
    , _filterModel(Q_NULLPTR)
    , _allData(Q_NULLPTR)
    , _searchDialog(Q_NULLPTR)
    , _isClosing(false)
    , _lastExportFilename(QStringLiteral("myParameters.txt"))
{
    ui->setupUi(this);

    initDefaultState();
    restoreAppearance();

    QDir binaryFilesDir(qApp->applicationDirPath());
#ifdef Q_OS_MAC
    binaryFilesDir.cdUp();
    binaryFilesDir.cd(QStringLiteral("Resources"));
#endif

    _db = QSqlDatabase::addDatabase(QLatin1String("QSQLITE"));
    QString sqlPath = QDir::cleanPath(binaryFilesDir.absoluteFilePath(QLatin1String("inscriptions.sqlite3")));
    _db.setDatabaseName(sqlPath);
    bool ok = _db.open();
    //ok = false;
    if (!ok)
    {
        qDebug() << "Failed to open database"<< sqlPath;
        ui->mainDisplay->append(tr("Failed to connect to the database. Your installation is probably corrupted."));
        return;
    }

    initValidState();

    _allData = new InscriptionTreeModel(_db, this);
    _signaturesListProxy = new InscriptionProxyModel(this);
    _signaturesListProxy->setSourceModel(_allData);
    ui->lstSignatures->setModel(_signaturesListProxy);
    ui->lstSignatures->header()->close();
    // Hide all the columns except of Signature
    for (int i = 1; i < _signaturesListProxy->columnCount(); i++)
    {
        ui->lstSignatures->setColumnHidden(i, true);
    }
    ui->lstSignatures->setItemDelegateForColumn(0, new InscriptionIdDelegate(ui->lstSignatures));

    connect(ui->lstSignatures->selectionModel(), SIGNAL(selectionChanged(QItemSelection, QItemSelection)),
            this, SLOT(onSignaturesSelectionChanged(QItemSelection, QItemSelection)));
    connect(_signaturesListProxy, SIGNAL(filterChanged()), this, SLOT(onSignaturesListChanged()));

    //////////////
    // Prepare search/filter menus and view
    ui->filtersView->setContextMenuPolicy(Qt::CustomContextMenu);
    connect(ui->filtersView, SIGNAL(customContextMenuRequested(const QPoint &)), this, SLOT(onCustomContextMenu(const QPoint &)));

    QSettings settings;
    QStringList header;
    header << tr("Operator") << tr("Parameter") << tr("Value") << tr("column name");
    _filterModel = new FilterModel(header, settings.value(QStringLiteral("filters")).toString());
    ui->filtersView->setModel(_filterModel);
    ui->filtersView->setRootIsDecorated(true);
    ui->filtersView->setColumnHidden(3, true);
    ui->actionExportParameters->setEnabled(_filterModel->rowCount() > 0);

    connect(ui->menuFilters, SIGNAL(aboutToShow()), this, SLOT(onFilterMenuAboutToShow()));
    connect(ui->chkUseFilter, SIGNAL(toggled(bool)), this, SLOT(onFilterStateToggle(bool)));
    connect(ui->filtersView, SIGNAL(doubleClicked(QModelIndex)), this, SLOT(onFilterViewDoubleClick(QModelIndex)));
    connect(ui->actionTextFormat, SIGNAL(triggered(bool)), this, SLOT(onCallTextFormat(bool)));
    connect(ui->filtersView->selectionModel(), SIGNAL(selectionChanged(QItemSelection,QItemSelection)),
            this, SLOT(onFilterSelectionChanged(QItemSelection,QItemSelection)));
    //////////////

    _searchDialog = new FilterDialog(_allData, this);
    auto crossesModel = new CrossesSqlTable(this, _db);
    crossesModel->setTable(QStringLiteral("cross_forms"));
    crossesModel->setEditStrategy(QSqlTableModel::OnManualSubmit);
    crossesModel->select();
    _searchDialog->setCrossesModel(crossesModel, 1);

    {
        // Load KMZ file with inscriptions and make a map between placemarks and inscriptions
        QString kmzFile = binaryFilesDir.absoluteFilePath(QStringLiteral("inscriptions.kmz"));
        qDebug() << "kmz file"<< kmzFile;
        if (QFile::exists(kmzFile))
        {
            auto manager = new ParsingRunnerManager(ui->mapWidget->model()->pluginManager(), this);
            _placemarks = manager->openFile(kmzFile);
            ui->mapWidget->model()->treeModel()->addDocument(_placemarks);
            auto placemarkList = _placemarks->placemarkList();
            for (int i = 0; i < placemarkList.size(); ++i)
            {
                auto item = placemarkList.at(i);
                _signature2placemark[item->name()] = i;
            }
        }
        else
        {
            ui->mapFrame->hide();
            ui->actionShowMap->setEnabled(false);
        }
    }

    // Load style for main inscription display
    _displayDocument = new QTextDocument(this);
    QFile styleFile(QStringLiteral(":/viewStyle.css"));
    styleFile.open(QIODevice::ReadOnly | QIODevice::Text);
    QTextStream ts(&styleFile);
    QString defaultStyle = ts.readAll();
    _displayDocument->setDefaultStyleSheet(defaultStyle);
    ui->mainDisplay->setDocument(_displayDocument);
    ui->mainDisplay->setStyleSheet(defaultStyle);

    restoreDisplayColumns();
    onFilterModelChange();
}

PanelWindow::~PanelWindow()
{
    if (_db.isOpen())
    {
        _db.close();
    }

    delete ui;
}

void PanelWindow::onSignaturesSelectionChanged(const QItemSelection &selected, const QItemSelection &deselected)
{
    Q_UNUSED(selected)

    foreach (auto index, deselected.indexes())
    {
        if (index.column() != 0)
            continue;

        auto row = index.row();
        if (index.parent().isValid())
            row = index.parent().row();
        auto tmpIndex = _signaturesListProxy->index(row, 0);
        auto indexInAllData = _signaturesListProxy->mapToSource(tmpIndex);
        auto signature = _allData->data(indexInAllData).toString();

        if (_signature2placemark.contains(signature))
            _placemarks->at(_signature2placemark[signature]).setVisible(false);
    }

    displaySignatureInfo();
}

void PanelWindow::onCustomContextMenu(const QPoint &point)
{
    auto index = ui->filtersView->indexAt(point);
    bool enableItemMenus = false;
    if (index.isValid())
    {
        enableItemMenus = true;
    }
    ui->actionAddSubparameter->setEnabled(enableItemMenus && (index.parent() == ui->filtersView->rootIndex()));
    ui->actionEditParameter->setEnabled(enableItemMenus);
    ui->actionRemoveParameter->setEnabled(enableItemMenus);
    ui->actionClearParameters->setEnabled(enableItemMenus);
    ui->menuFilters->exec(ui->filtersView->mapToGlobal(point));
    ui->actionExportParameters->setEnabled(ui->filtersView->model()->rowCount() > 0);
}

void PanelWindow::onSignaturesListChanged()
{
    ui->status->setText(tr("Number of available inscriptions: %1").arg(_signaturesListProxy->rowCount()));
}

void PanelWindow::onFilterMenuAboutToShow()
{
    auto index = ui->filtersView->currentIndex();
    bool en = index.isValid();
    ui->actionAddSubparameter->setEnabled(en && (index.parent() == ui->filtersView->rootIndex()));
    ui->actionEditParameter->setEnabled(en);
    ui->actionRemoveParameter->setEnabled(en);

    en = ui->filtersView->model()->rowCount() > 0;
    ui->actionClearParameters->setEnabled(en);
    ui->actionExportParameters->setEnabled(en);
}

void PanelWindow::on_actionAddNewParameter_triggered()
{
    if (_searchDialog == Q_NULLPTR || _filterModel == Q_NULLPTR)
        return;

    _searchDialog->reset();
    int retCode = _searchDialog->exec();
    if (retCode == QDialog::Rejected)
        return;

    auto model = ui->filtersView->model();
    int newRow = model->rowCount(); // always append
    auto index = ui->filtersView->rootIndex().parent();
    if (!model->insertRow(newRow, index))
    {
        return;
    }

    auto newItem = model->index(newRow, 0, index);
    model->setData(newItem, _searchDialog->condition());

    newItem = model->index(newRow, 1, index);
    model->setData(newItem, _searchDialog->searchField());
    newItem = model->index(newRow, 2, index);
    model->setData(newItem, _searchDialog->searchValue());
    newItem = model->index(newRow, 3, index);
    model->setData(newItem, _searchDialog->columnName());

    QItemSelection selection(model->index(newRow, 0, index), model->index(newRow, model->columnCount(), index));
    ui->filtersView->selectionModel()->select(selection, QItemSelectionModel::ClearAndSelect);

    onFilterModelChange();
}

void PanelWindow::on_actionAddSubparameter_triggered()
{
    int retCode = _searchDialog->exec();
    if (retCode == QDialog::Rejected)
        return;

    auto model = ui->filtersView->model();
    auto index = ui->filtersView->selectionModel()->currentIndex();
    int newRow = model->rowCount(index);
    if (!_filterModel->insertRow(newRow, index))
    {
        return;
    }

    QModelIndex zeroIndex = model->index(index.row(), 0, index.parent());
    QModelIndex newItem = model->index(newRow, 0, zeroIndex);
    model->setData(newItem, _searchDialog->condition());

    newItem = model->index(newRow, 1, zeroIndex);
    model->setData(newItem, _searchDialog->searchField());
    newItem = model->index(newRow, 2, zeroIndex);
    model->setData(newItem, _searchDialog->searchValue());
    newItem = model->index(newRow, 3, zeroIndex);
    model->setData(newItem, _searchDialog->columnName());

    QItemSelection selection(model->index(newRow, 0, zeroIndex), model->index(newRow, model->columnCount(), zeroIndex));
    ui->filtersView->selectionModel()->select(selection, QItemSelectionModel::ClearAndSelect);
    ui->filtersView->expand(zeroIndex);
    onFilterModelChange();
}

void PanelWindow::on_actionRemoveParameter_triggered()
{
    auto index = ui->filtersView->selectionModel()->currentIndex();
    auto model = ui->filtersView->model();
    model->removeRow(index.row(), index.parent());

    onFilterModelChange();
}

void PanelWindow::on_actionClearParameters_triggered()
{
    auto model = ui->filtersView->model();
    model->removeRows(0, model->rowCount());
    onFilterModelChange();

    ui->chkUseFilter->setChecked(false);
}

void PanelWindow::on_actionEditParameter_triggered()
{
    auto index = ui->filtersView->selectionModel()->currentIndex();
    if (!index.isValid())
        return;

    auto dataIndex = _filterModel->index(index.row(), 1, index.parent());
    _searchDialog->setSearchField(_filterModel->data(dataIndex, Qt::EditRole).toString());
    dataIndex = _filterModel->index(index.row(), 2, index.parent());
    _searchDialog->setSearchValue(_filterModel->data(dataIndex, Qt::EditRole).toString());
    dataIndex = _filterModel->index(index.row(), 0, index.parent());
    _searchDialog->setCondition(_filterModel->data(dataIndex, Qt::EditRole).toString());

    int retCode = _searchDialog->exec();
    if (retCode == QDialog::Rejected)
        return;

    dataIndex = _filterModel->index(index.row(), 0, index.parent());
    _filterModel->setData(dataIndex, _searchDialog->condition());
    dataIndex = _filterModel->index(index.row(), 1, index.parent());
    _filterModel->setData(dataIndex, _searchDialog->searchField());
    dataIndex = _filterModel->index(index.row(), 2, index.parent());
    _filterModel->setData(dataIndex, _searchDialog->searchValue().toString());
    dataIndex = _filterModel->index(index.row(), 3, index.parent());
    _filterModel->setData(dataIndex, _searchDialog->columnName());

    onFilterModelChange();
}

void PanelWindow::onFilterModelChange()
{
    QString sqlCondition = QString();
    QString yes = QStringLiteral("yes");
    QString no = QStringLiteral("no");

    if (_filterModel == Q_NULLPTR)
        return;

    bool filterPresent = _filterModel->rowCount() > 0;
    ui->actionExportParameters->setEnabled(filterPresent);
    ui->chkUseFilter->setEnabled(filterPresent);
    if (!filterPresent)
    {
        _signaturesListProxy->setEnabled(false);
        ui->chkUseFilter->setChecked(false);
        return;
    }

    for (int i = 0; i < _filterModel->rowCount(); i++)
    {
        bool isCross = false;
        QString operatorName = _filterModel->data(_filterModel->index(i, 0), Qt::EditRole).toString();
        QString columnName = _filterModel->data(_filterModel->index(i, 3), Qt::EditRole).toString();
        QString value = _filterModel->data(_filterModel->index(i, 2), Qt::EditRole).toString();
        // sql query for one group (parent and it's children)
        QString groupCondition;
        QStringList crossForms;
        QStringList crossFormsOperator;

        if (columnName == QLatin1String("crosses"))
        {
            crossForms << value;
            crossFormsOperator << operatorName;
            isCross = true;
        }

        if (columnName == QLatin1String("all_data.new_reading")
                || columnName == QLatin1String("all_data.lost"))
        {
            if (value.compare(yes, Qt::CaseInsensitive) == 0)
                value = QStringLiteral("1");
            else if (value.compare(no, Qt::CaseInsensitive) == 0)
                value = QStringLiteral("0");
        }

        // one operator condition
        QString condition;
        if (value.contains(QLatin1Char('*')))
        {
            value.replace(QLatin1Char('*'), QLatin1Char('%'));
            condition = QStringLiteral("%1 LIKE '%2'").arg(columnName).arg(value);
        }
        else
        {
            condition = QStringLiteral("%1 = '%2'").arg(columnName).arg(value);
            if (isCross)
            {
                condition.clear();
            }
        }

        if (columnName == QLatin1String("hasAlternatives"))
        {
            condition.clear();
            if (value.compare(yes, Qt::CaseInsensitive) == 0)
            {
                condition.append(QStringLiteral("signatureId IN (SELECT id FROM signatures WHERE id NOT IN (SELECT parent_id FROM signatures WHERE parent_id IS NOT NULL))"));
            }
            else if (value.compare(no, Qt::CaseInsensitive) == 0)
            {
                condition.append(QStringLiteral("signatureId IN (SELECT parent_id FROM signatures WHERE parent_id IS NOT NULL)"));
            }
        }

        QModelIndex parent = _filterModel->index(i, 0);
        if (_filterModel->hasChildren(parent))
        {
            groupCondition = QStringLiteral(" %1 (%2").arg(operatorName).arg(condition);
            bool isEmpty = true;
            for (int j = 0; j < _filterModel->rowCount(parent); j++)
            {
                operatorName = _filterModel->data(_filterModel->index(j, 0, parent), Qt::EditRole).toString();
                columnName = _filterModel->data(_filterModel->index(j, 3, parent), Qt::EditRole).toString();
                value = _filterModel->data(_filterModel->index(j, 2, parent), Qt::EditRole).toString();
                if (columnName == QLatin1String("crosses"))
                {
                    crossForms << value;
                    crossFormsOperator << operatorName;
                    continue;
                }
                isEmpty = false;

                if (columnName == QLatin1String("hasAlternatives"))
                {
                    if (value.compare(yes, Qt::CaseInsensitive) == 0)
                    {
                        groupCondition.append(QStringLiteral("signatureId IN (SELECT id FROM signatures WHERE id NOT IN (SELECT parent_id FROM signatures WHERE parent_id IS NOT NULL))"));
                    }
                    else if (value.compare(no, Qt::CaseInsensitive) == 0)
                    {
                        groupCondition.append(QStringLiteral("signatureId IN (SELECT parent_id FROM signatures WHERE parent_id IS NOT NULL)"));
                    }
                    continue;
                }

                if (columnName == QLatin1String("all_data.new_reading")
                        || columnName == QLatin1String("all_data.lost"))
                {
                    if (value.compare(yes, Qt::CaseInsensitive) == 0)
                        value = QStringLiteral("1");
                    else if (value.compare(no, Qt::CaseInsensitive) == 0)
                        value = QStringLiteral("0");
                }

                if (value.contains(QLatin1Char('*')))
                {
                    value.replace(QLatin1Char('*'), QLatin1Char('%'));
                    groupCondition += QStringLiteral(" %1 %2 LIKE '%3'").arg(operatorName).arg(columnName).arg(value);
                }
                else
                {
                    groupCondition += QStringLiteral(" %1 %2 = '%3'").arg(operatorName).arg(columnName).arg(value);
                }
            }
            groupCondition += QLatin1Char(')');
            if (isEmpty && isCross)
            {
                // this search group consists of crosses entirely
                groupCondition.clear();

                for (int j = 0; j < crossForms.length(); ++j)
                {
                    QString op = crossFormsOperator.at(j);
                    //groupCondition += QString("%1 name = '%2'").arg(op).arg(crossForms.at(j));
                    groupCondition += QStringLiteral(" %1 (all_data.id IN (SELECT meta_id from meta_with_cross_forms WHERE name = '%2') and num_crosses > 0)")
                            .arg(op).arg(crossForms.at(j));
                    qDebug() << "groupCondition iteration " << j << ", " << groupCondition;
                }
            }
            else if (isEmpty || isCross)
            {
                // we have main parent non cross and all children crosses
                // remove the last character
                groupCondition.chop(1);
                for (int j = 0; j < crossForms.length(); ++j)
                {
                    groupCondition += QStringLiteral(" %1 (all_data.id IN (SELECT meta_id FROM meta_with_cross_forms WHERE name = '%2') and num_crosses > 0)").arg(crossFormsOperator.at(j))
                            .arg(crossForms.at(j));
                }
                groupCondition += QLatin1Char(')');
            }
            qDebug() << "groupCondition after loop: " << groupCondition;
        }
        else
        {
            if (isCross)
            {
                condition = QStringLiteral("all_data.id IN (SELECT meta_id FROM meta_with_cross_forms WHERE name = '%1')")
                        .arg(crossForms.at(0));
                groupCondition = QStringLiteral(" ( %1 (%2) and num_crosses > 0)").arg(operatorName).arg(condition);
            }
            else
                groupCondition += QStringLiteral(" %1 (%2)").arg(operatorName).arg(condition);

            qDebug() << "groupCondition: " << groupCondition;
        }

        if (!sqlCondition.isEmpty() && groupCondition.at(1) == QLatin1Char('('))
        {
            // we need to adjust '('
            groupCondition.remove(0, 2);
            int newPos = groupCondition.indexOf(QLatin1Char('('), 0);
            groupCondition.insert(newPos, QLatin1Char('('));
        }
        sqlCondition += groupCondition;
        qDebug() << "sqlCondition: " << sqlCondition;
    }
    // remove the very first operator
    int firstOperator;
    if ((firstOperator = sqlCondition.indexOf(QLatin1String("AND"), 0)) != -1)
    {
        sqlCondition.remove(firstOperator-1, 4);
    } else if ((firstOperator = sqlCondition.indexOf(QLatin1String("OR"), 0)) != -1)
    {
        sqlCondition.remove(firstOperator-1, 3);
    }

    sqlCondition += QStringLiteral(" GROUP BY id");
    qDebug() << "Final sql:" << sqlCondition;

    auto validIds = _allData->signaturesForQuery(sqlCondition);
    _signaturesListProxy->setValidIndices(validIds);
}

void PanelWindow::onFilterStateToggle(bool enabled)
{
    _signaturesListProxy->setEnabled(enabled);
}

void PanelWindow::onFilterViewDoubleClick(const QModelIndex &index)
{
    if (!index.isValid())
        return;

    QModelIndex dataIndex = _filterModel->index(index.row(), 1, index.parent());
    _searchDialog->setSearchField(_filterModel->data(dataIndex, Qt::EditRole).toString());
    dataIndex = _filterModel->index(index.row(), 2, index.parent());
    _searchDialog->setSearchValue(_filterModel->data(dataIndex, Qt::EditRole).toString());
    dataIndex = _filterModel->index(index.row(), 0, index.parent());
    _searchDialog->setCondition(_filterModel->data(dataIndex, Qt::EditRole).toString());

    int retCode = _searchDialog->exec();
    if (retCode == QDialog::Rejected)
        return;

    dataIndex = _filterModel->index(index.row(), 0, index.parent());
    _filterModel->setData(dataIndex, _searchDialog->condition());
    dataIndex = _filterModel->index(index.row(), 1, index.parent());
    _filterModel->setData(dataIndex, _searchDialog->searchField());
    dataIndex = _filterModel->index(index.row(), 2, index.parent());
    _filterModel->setData(dataIndex, _searchDialog->searchValue().toString());
    dataIndex = _filterModel->index(index.row(), 3, index.parent());
    _filterModel->setData(dataIndex, _searchDialog->columnName());

    onFilterModelChange();
}

void PanelWindow::onFilterSelectionChanged(const QItemSelection &selected, const QItemSelection &deselected)
{
    Q_UNUSED(deselected)
    Q_UNUSED(selected)
    onFilterMenuAboutToShow();
}

void PanelWindow::onCallTextFormat(bool)
{
    auto model = _allData->displayableColumns();
    auto frmt = new FormatSelectorDialog(this);
    frmt->setModal(true);
    frmt->setModel(model);
    int retCode = frmt->exec();
    if (retCode == QDialog::Rejected)
        return;

    displaySignatureInfo();
}

void PanelWindow::saveState()
{
    QSettings settings;
    if (_filterModel != Q_NULLPTR)
    {
        QString forSaving = _filterModel->toString();
        settings.setValue(QStringLiteral("filters"), forSaving);

        QString inscriptionInfoFields;
        QTextStream infoStream(&inscriptionInfoFields);
        auto model = _allData->displayableColumns();
        for (int i = 0; i < model->rowCount(); ++i)
        {
            QModelIndex idx = model->index(i, 0);
            if (!model->data(idx, InscriptionTreeModel::SelectedRole).toBool())
                continue;

            infoStream << model->data(idx, InscriptionTreeModel::ColumnNameRole).toString() << '\t';
            infoStream << model->data(idx, InscriptionTreeModel::SelectedRole).toBool();
            infoStream << endl;
        }
        settings.setValue(QStringLiteral("inscriptionInfo"), inscriptionInfoFields);
        settings.setValue(QStringLiteral("showColumnNames"), model->data(model->index(0, 0), InscriptionTreeModel::ShowColumnNameRole).toBool() ? 1 : 0);
    }

    settings.setValue(QStringLiteral("mainWindow/size"), size());
    settings.setValue(QStringLiteral("mainWindow/pos"), pos());
    settings.setValue(QStringLiteral("mapFrame/visible"), ui->mapFrame->isVisible());

    settings.setValue(QStringLiteral("splitters/small"), ui->splitter->saveState());
    settings.setValue(QStringLiteral("splitters/big"), ui->splitter_2->saveState());
}

void PanelWindow::restoreAppearance()
{
    QSettings settings;
    QVariant mapVisibility = settings.value(QStringLiteral("mapFrame/visible"), true);
    if (mapVisibility.isValid())
    {
        ui->actionShowMap->setChecked(mapVisibility.toBool());
        ui->mapFrame->setVisible(mapVisibility.toBool());
    }
    ui->splitter->restoreState(settings.value(QStringLiteral("splitters/small")).toByteArray());
    ui->splitter_2->restoreState(settings.value(QStringLiteral("splitters/big")).toByteArray());

    settings.beginGroup(QStringLiteral("mainWindow"));
    resize(settings.value(QStringLiteral("size"), QSize(1280, 720)).toSize());
    move(settings.value(QStringLiteral("pos"), QPoint(100, 100)).toPoint());
    settings.endGroup();

    auto screenSize = QApplication::desktop()->availableGeometry();
    auto appSize = size();
    if (screenSize.height() < appSize.height()) {
        resize(QSize(appSize.width(), screenSize.height()));
        appSize = size();
    }
    if (screenSize.width() < appSize.width()) {
        resize(QSize(screenSize.width(), appSize.height()));
        appSize = size();
    }
    QRect appRect(pos(), size());
    if (!screenSize.contains(appRect))
    {
        move(QPoint(0, 0));
    }
}

void PanelWindow::createKml()
{
    const char kmlTag_nameSpaceOgc22[] = "http://www.opengis.net/kml/2.2";

    auto placemarks = new GeoDataDocument();
    int colLatitude = _allData->columnIndexByName(QStringLiteral("all_data.latitude"));
    int colLongitude = _allData->columnIndexByName(QStringLiteral("all_data.longitude"));
    int colSignature = _allData->columnIndexByName(QStringLiteral("signature"));

    GeoDataIconStyle iconStyle;
    iconStyle.setIconPath(QStringLiteral(":/rundata-16.png"));

    for (int i = 0; i < _signaturesListProxy->rowCount(_signaturesListProxy->index(-1, -1)); i++)
    {
        QModelIndex dataIndex = _signaturesListProxy->index(i, colLatitude);
        double latitude = _signaturesListProxy->data(dataIndex).toDouble();
        dataIndex = _signaturesListProxy->index(i, colLongitude);
        double longitude = _signaturesListProxy->data(dataIndex).toDouble();
        dataIndex = _signaturesListProxy->index(i, colSignature);
        QString name = _signaturesListProxy->data(dataIndex).toString();

        //qDebug() << QStringLiteral("%1, %2 - %3").arg(name).arg(longitude).arg(latitude);
        if (latitude == 0 || longitude == 0)
            continue;

        auto place = new GeoDataPlacemark(name);
        place->setCoordinate(longitude, latitude, 0., GeoDataCoordinates::Degree);
        place->setVisible(false); // all placemarks are hidden by default

        QSharedPointer<GeoDataStyle> myStyle(new GeoDataStyle);
        myStyle->setIconStyle(iconStyle);
        myStyle->balloonStyle().setDisplayMode(GeoDataBalloonStyle::Hide);
        place->setStyle(myStyle);

        placemarks->append(place);
    }

    QString dataFolder = QStandardPaths::writableLocation(QStandardPaths::AppLocalDataLocation);
    QDir dirData(dataFolder);
    if (!dirData.exists())
    {
        qDebug() << "Will try to create folder" << dataFolder;
        if (!dirData.mkpath(dataFolder))
            qDebug() << "Failed to create folder";

    }
    qDebug() << "Will write KML file into" << dataFolder;

    QFile out(QStringLiteral("%1/inscriptions.kml").arg(dataFolder));
    out.open(QIODevice::WriteOnly | QIODevice::Text);

    GeoWriter writer;
    writer.setDocumentType(QLatin1String(kmlTag_nameSpaceOgc22));
    writer.write(&out, placemarks);
    out.close();
}

/**
 * Set overll availability of user controls
 */
void PanelWindow::initDefaultState()
{
    ui->status->setText(QString());
    ui->status->setVisible(true);
    ui->chkUseFilter->setEnabled(false);

    ui->actionAddSubparameter->setEnabled(false);
    ui->actionEditParameter->setEnabled(false);
    ui->actionRemoveParameter->setEnabled(false);
    ui->actionClearParameters->setEnabled(false);
    ui->actionExportParameters->setEnabled(false);
    ui->actionAddNewParameter->setEnabled(false);
    ui->actionImportParameters->setEnabled(false);
    ui->actionTextFormat->setEnabled(false);

    {
        auto homeLatitude = 57.6; // somewhere on Gotland
        auto homeLongitude = 18.55;
        auto defaultZoom = 1736;
        ui->mapWidget->setProjection(Marble::Mercator);
        ui->mapWidget->setMapThemeId(QStringLiteral("earth/wikimedia/wikimedia.dgml"));
        ui->mapWidget->setShowScaleBar(false);
        ui->mapWidget->setShowCompass(false);
        ui->mapWidget->setShowOverviewMap(false);
        ui->mapWidget->setShowClouds(false);
        ui->mapWidget->setZoom(defaultZoom);
        ui->mapWidget->setShowGrid(false);
        ui->mapWidget->setCenterLatitude(homeLatitude);
        ui->mapWidget->setCenterLongitude(homeLongitude);
        QStringList floatItemsToDisable;
        floatItemsToDisable << QStringLiteral("elevationprofile") << QStringLiteral("GpsInfo")
          << QStringLiteral("progress") << QStringLiteral("routing") << QStringLiteral("speedometer");
        foreach (auto itemName, floatItemsToDisable) {
            auto floatItem = ui->mapWidget->floatItem(itemName);
            if (floatItem)
                floatItem->hide();
        }
        ui->mapWidget->model()->setHome(homeLongitude, homeLatitude, defaultZoom);
    }

    connect(ui->actionAboutQt, SIGNAL(triggered()), qApp, SLOT(aboutQt()));

    ui->actionAddNewParameter->setShortcuts(QKeySequence::Find);
    ui->actionAddSubparameter->setShortcut(QKeySequence::New);

    // Backspace is needed on Mac, since Backspace is not handled
    // as 'Delete' (at least on Macbook Air) despite what seems to be a correct
    // representation to a user, i.e. menu shows <- as action, user hits <-,
    // but nothing happens.
    // For other platforms we will have two keys. Laptops tend to miss Delete key.
    ui->actionRemoveParameter->setShortcuts(QList<QKeySequence>()
        << QKeySequence::Delete << tr("Backspace"));

    ui->actionClearParameters->setShortcuts(QKeySequence::Close);
    ui->actionEditParameter->setShortcut(tr("Ctrl+E"));
    ui->actionShowMap->setShortcut(tr("Ctrl+M"));
}

void PanelWindow::initValidState()
{
    ui->actionImportParameters->setEnabled(true);
    ui->actionTextFormat->setEnabled(true);
    ui->actionAddNewParameter->setEnabled(true);
    ui->chkUseFilter->setEnabled(true);
}

void PanelWindow::displaySignatureInfo()
{
    const QString paragraphSymbol = QLatin1String("§");

    ui->mainDisplay->clear();

    // get original qmodelindex from signatures proxy
    // get data from dbc->modelForFiltering for those indices
    //QAbstractTableModel *allDataModel = _dbc->modelForFiltering();
    QModelIndex indexInAllData;
    int placemarkRow = -1;

    auto indexes = ui->lstSignatures->selectionModel()->selectedIndexes();

    auto displayModel = _allData->displayableColumns();
    auto start = displayModel->index(0, 0);
    auto displayedColumns = displayModel->match(start, InscriptionTreeModel::SelectedRole, true, -1);
    bool displayHeaders = displayModel->data(start, InscriptionTreeModel::ShowColumnNameRole).toBool();

    QString finalText;
    foreach (QModelIndex index, indexes)
    {
        // process row-based
        if (index.column() != 0)
            continue;

        int row = index.row();
        if (index.parent().isValid())
            row = index.parent().row();
        QModelIndex tmpIndex = _signaturesListProxy->index(row, 0);
        indexInAllData = _signaturesListProxy->mapToSource(tmpIndex);
        QString signature = _allData->data(indexInAllData).toString();
        if (_signature2placemark.contains(signature))
        {
            _placemarks->at(_signature2placemark[signature]).setVisible(true);
            if (placemarkRow == -1)
                placemarkRow = _signature2placemark[signature];
        }

        QString paragraph;
        foreach (QModelIndex columnIndex, displayedColumns)
        {
            QString columnName = displayModel->data(columnIndex, InscriptionTreeModel::ColumnNameRole).toString();
            QString columnHumanName = displayModel->data(columnIndex).toString();
            int column = _allData->columnIndexByName(columnName);
            QModelIndex dataIndex = _allData->index(indexInAllData.row(), column);
            QString columnData = _allData->data(dataIndex).toString();

            //ui->mainDisaply->moveCursor(QTextCursor::End);
            if (paragraph.length() > 0 && !displayHeaders)
            {
                //ui->mainDisaply->insertPlainText(QString("\n<br/>"));
                paragraph.append(QStringLiteral("<br>"));
            }
            if (displayHeaders)
            {
                //ui->mainDisaply->insertHtml(QString("<p>%1: ").arg(columnHumanName));
                //ui->mainDisaply->moveCursor(QTextCursor::End);
                //finalText = finalText + QString("%1: ").arg(columnHumanName);
                paragraph.append(QStringLiteral("<h5>%1</h5>").arg(columnHumanName));
            }

            if (columnName.contains(QLatin1String("transliteration")) || columnName.contains(QLatin1String("english_translation"))
                    || columnName.contains(QLatin1String("scandinavian_normalization"))
                    || columnName.contains(QLatin1String("norse_normalization")))
            {
                QString cssStyleClass = QStringLiteral("normalization");
                if (columnName.contains(QLatin1String("transliteration")))
                    cssStyleClass = QStringLiteral("transliteration");
                else if (columnName.contains(QLatin1String("english_translation"))) {
                    cssStyleClass = QString();
                }

                // we need to display it in bold
                if (columnData.contains(paragraphSymbol))
                {
                    auto parts = columnData.split(paragraphSymbol, QString::SkipEmptyParts);

                    QString listHeader;
                    if (columnData.contains(QLatin1String("§A")))
                        listHeader = tr("Sides:");
                    if (columnData.contains(QStringLiteral("§P")))
                        listHeader = tr("Reading variants:");


                    paragraph.append(listHeader + QStringLiteral("<ol type=\"A\">"));
                    foreach (auto side, parts) {
                        QString sdata;
                        // be sure not to crash if transliteration is short/missing
                        if (side.length() > 2)
                            sdata = side.mid(2);

                        paragraph.append(QStringLiteral("<li>&nbsp;<span class=\"%1\">").arg(cssStyleClass) + sdata + QStringLiteral("</span></li>"));
                    }
                    paragraph.append(QStringLiteral("</ol>"));
                }
                else
                {
                    paragraph.append(QStringLiteral("<span class=\"%1\">").arg(cssStyleClass) + columnData + QStringLiteral("</span>"));
                }

                continue;
            }

            if (columnName == QLatin1String("crosses"))
            {
                column = _allData->columnIndexByName(QStringLiteral("all_data.num_crosses"));
                dataIndex = _allData->index(indexInAllData.row(), column);
                int numCrosses = _allData->data(dataIndex).toInt();
                paragraph.append(tr("Contains %1 cross(es)").arg(numCrosses));
                if (numCrosses == 0)
                    continue;
                paragraph.append(QStringLiteral("<table class=\"crosses\" border=\"1\">"));
                paragraph.append(QStringLiteral("<thead>\n<tr>\n"));
                paragraph.append(QStringLiteral("\t<th>A</th>\n"));
                paragraph.append(QStringLiteral("\t<th>B</th>\n"));
                paragraph.append(QStringLiteral("\t<th>C</th>\n"));
                paragraph.append(QStringLiteral("\t<th>D</th>\n"));
                paragraph.append(QStringLiteral("\t<th>E</th>\n"));
                paragraph.append(QStringLiteral("\t<th>F</th>\n"));
                paragraph.append(QStringLiteral("\t<th>G</th>\n"));
                paragraph.append(QStringLiteral("</tr></thead>\n"));
                paragraph.append(QStringLiteral("<tbody>\n"));

                auto allCrosses = _allData->crosses(indexInAllData);
                foreach (auto oneCross, allCrosses) {
                    if (!oneCross.at(0).isEmpty())
                    {
                        paragraph.append(QStringLiteral("<tr><td colspan=\"7\">%1</td></tr>\n").arg(oneCross.at(0).at(0)));
                        continue;
                    }
                    paragraph.append(QStringLiteral("<tr>\n"));

                    // we have 8 style groups in total, 0 being free-text and not a group
                    for (int i = 1; i < 8; ++i)
                    {
                        const QStringList crossForms = oneCross.at(i);
                        if (crossForms.isEmpty())
                        {
                            paragraph.append(QStringLiteral("<td><span class=\"null\">&#8709;</span></td>"));
                            continue;
                        }

                        paragraph.append(QStringLiteral("<td>"));
                        foreach (auto oneForm, crossForms) {
                            paragraph.append(QStringLiteral("<img src=\":/images/crosses/%1\" alt=\"%1\" title=\"%1\" width=\"32\" height=\"32\">, ").arg(oneForm));
                        }
                        paragraph.chop(2);
                        paragraph.append(QStringLiteral("</td>\n"));
                    }
                    paragraph.append(QStringLiteral("</tr>\n"));
                }
                paragraph.append(QStringLiteral("</tbody></table>\n"));
            }

            //ui->mainDisaply->insertHtml(textToDisplay);
            //ui->mainDisaply->insertHtml("</p>");
            //ui->mainDisaply->moveCursor(QTextCursor::End);
            paragraph.append(columnData);
        }
        finalText = finalText + QStringLiteral("<p>%1</p>\n").arg(paragraph);
    }
    ui->mainDisplay->setHtml(QStringLiteral("<body>") + finalText + QStringLiteral("</body>"));

    if (placemarkRow != -1)
    {
        auto feat = &_placemarks->at(placemarkRow);
        if (const auto placemark = geodata_cast<GeoDataPlacemark>(feat))
        {
            ui->mapWidget->centerOn(placemark->coordinate());
            ui->mapWidget->zoomView(2300);
        }
    }
    ui->mapWidget->repaint();
}

void PanelWindow::restoreDisplayColumns()
{
    QSettings settings;
    QString in(settings.value(QStringLiteral("inscriptionInfo")).toString());
    if (in.isEmpty())
        return;
    if (_allData == Q_NULLPTR)
        return;

    QStandardItemModel *model = qobject_cast<QStandardItemModel*>(_allData->displayableColumns());
    if (model == Q_NULLPTR)
        return;

    const QStringList lines = in.split(QLatin1Char('\n'));
    int selectedIdx = 0;
    auto modelStartIdx = model->index(0, 0);
    foreach (auto line, lines)
    {
        const QStringList fields = line.split(QLatin1Char('\t'));
        if (line.isEmpty() || fields.size() < 2)
            continue;

        const QString dbName = fields[0];
        const bool selected = fields[1] == QLatin1String("1") ? 1 : 0;
        if (selected)
        {
            auto foundItems = model->match(modelStartIdx, InscriptionTreeModel::ColumnNameRole, dbName);
            if (foundItems.isEmpty())
            {
                qDebug() << "something went wrong";
                continue;
            }
            auto row = foundItems[0].row();
            auto movedItem = model->itemFromIndex(foundItems[0]);
            movedItem->setData(true, InscriptionTreeModel::SelectedRole);
            movedItem->setData(true, InscriptionTreeModel::TemporarySelectedRole);
            model->takeRow(row);
            model->insertRow(selectedIdx++, movedItem);
        }
    }
    bool showHeader = settings.value(QStringLiteral("showColumnNames")).toInt() == 1 ? true : false;
    model->setData(model->index(0, 0), showHeader, InscriptionTreeModel::ShowColumnNameRole);
}

void PanelWindow::on_actionReload_style_triggered()
{
    QString fileName = QFileDialog::getOpenFileName(this, QStringLiteral("File with style"), QString(), tr("Any file (*.css)"));
    if (fileName.isEmpty() || fileName.isNull())
        return;

    QFile styleFile(fileName);
    styleFile.open(QIODevice::ReadOnly | QIODevice::Text);
    QTextStream ts(&styleFile);
    QString newStyle = ts.readAll();

    _displayDocument->setDefaultStyleSheet(newStyle);
    ui->mainDisplay->setStyleSheet(newStyle);
    ui->mainDisplay->repaint();
}

void PanelWindow::on_actionShowMap_triggered(bool checked)
{
    ui->mapFrame->setVisible(checked);
}


void PanelWindow::closeEvent(QCloseEvent *event)
{
    if (_isClosing)
    {
        event->accept();
        return;
    }

    _isClosing = true;
    saveState();
    event->accept();
}

void PanelWindow::on_actionExportParameters_triggered()
{
    if (_filterModel == Q_NULLPTR)
        return;

    auto fileName = QFileDialog::getSaveFileName(this, tr("Export search parameters"),
                        _lastExportFilename,
                        tr("Text files (*.txt)"));
    if (fileName.isEmpty())
        return;

    _lastExportFilename = fileName;
    qDebug() << "export to" << _lastExportFilename;

    QSaveFile fileToSave(fileName, this);
    if (!fileToSave.open(QIODevice::WriteOnly | QIODevice::Truncate))
    {
        qDebug() << "Failed to open export file for writing";
        return;
    }

    QTextStream res(&fileToSave);
    res.setGenerateByteOrderMark(true);
    res << _filterModel->toString();
    fileToSave.commit();
    qDebug() << "export done";
}

void PanelWindow::on_actionImportParameters_triggered()
{
    if (_filterModel == Q_NULLPTR)
        return;

    auto fileName = QFileDialog::getOpenFileName(this, tr("Import search parameters"),
                        _lastExportFilename,
                        tr("Text files (*.txt)"));
    if (fileName.isEmpty())
        return;

    QFile paramsFile(fileName);
    paramsFile.open(QIODevice::ReadOnly | QIODevice::Text);
    QTextStream ts(&paramsFile);
    QString header = ts.readLine();
    if (!header.contains(QStringLiteral("searchParameters")))
    {
        QMessageBox::warning(this, tr("Error"), tr("Selected files doesn't contain search parameters."));
        return;
    }
    if (!ts.seek(0))
    {
        return;
    }

    _filterModel->fromString(ts.readAll());
}

void PanelWindow::on_actionAbout_triggered()
{
    AboutDialog dialog(AboutDialog::ABOUT, this);
    dialog.exec();
}
