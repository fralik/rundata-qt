//
// This file is part of Rundata-qt.
//
// This program is free software licensed under the GNU GPL v3 or any later version.
// You can find a copy of this license in LICENSE.GPLv3 in the top directory of
// the source code.
//
// Copyright 2017 Vadim Frolov <fralik@gmail.com>
//
#ifndef INSCRIPTION_ID_DELEGATE_H
#define INSCRIPTION_ID_DELEGATE_H

#include <QObject>
#include <QStyledItemDelegate>

/**
 * @brief The InscriptionIdDelegate class is used to apply custom style for QTreeView items based on dynamic properties.
 *
 * The class is intendended to work with SignatureTreeModel. It checks the following roles of an item:
 * 1. SignatureTreeModel::NewReadingRole. If true, sets font to bold.
 */
class InscriptionIdDelegate : public QStyledItemDelegate
{
    Q_OBJECT

public:
    explicit InscriptionIdDelegate(QObject *parent = Q_NULLPTR);

    // QAbstractItemDelegate interface
public:
    void paint(QPainter *painter, const QStyleOptionViewItem &option, const QModelIndex &index) const Q_DECL_OVERRIDE;
};

#endif // INSCRIPTION_ID_DELEGATE_H
