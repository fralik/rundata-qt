//
// This file is part of Rundata-qt.
//
// This program is free software licensed under the GNU GPL v3 or any later version.
// You can find a copy of this license in LICENSE.GPLv3 in the top directory of
// the source code.
//
// Copyright 2017 Vadim Frolov <fralik@gmail.com>
//
#include "FormatSelectorDialog.h"
#include "ui_FormatSelectorDialog.h"
#include "FormatListProxy.h"
#include "InscriptionTreeModel.h"

#include <QAbstractItemModel>
#include <QListWidgetItem>
#include <QDebug>
#include <QStandardItemModel>

FormatSelectorDialog::FormatSelectorDialog(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::FormatSelectorDialog)
    , _model(Q_NULLPTR)
{
    ui->setupUi(this);
    _availableProxy = new FormatListProxy(this);
    _selectedProxy = new FormatListProxy(this);
    _selectedProxy->setProxyCondition(true);

    _availableProxy->setProxyCondition(false);
    _availableProxy->setSortCaseSensitivity(Qt::CaseInsensitive);
    _availableProxy->setSortRole(Qt::DisplayRole);

    ui->listAvailable->setEditTriggers(QAbstractItemView::NoEditTriggers);
    ui->listAvailable->setSelectionMode(QAbstractItemView::ExtendedSelection);

    ui->listSelected->setEditTriggers(QAbstractItemView::NoEditTriggers);
    ui->listSelected->setDragEnabled(true);
    ui->listSelected->setDropIndicatorShown(true);
    ui->listSelected->setDragDropMode(QAbstractItemView::DragDrop);
    ui->listSelected->setDefaultDropAction(Qt::MoveAction);
    ui->listSelected->setSelectionMode(QAbstractItemView::ExtendedSelection);

    connect(ui->listAvailable, SIGNAL(doubleClicked(QModelIndex)),
            this, SLOT(onAvailableDoubleClick(QModelIndex)));
    connect(ui->listSelected, SIGNAL(doubleClicked(QModelIndex)),
            this, SLOT(onSelectedDoubleClick(QModelIndex)));
    connect(ui->btnAdd, SIGNAL(clicked()), this, SLOT(onAddButtonClick()));
    connect(ui->btnAddAll, SIGNAL(clicked()), this, SLOT(onAddAllButtonClick()));
    connect(ui->btnRemove, SIGNAL(clicked()), this, SLOT(onRemoveButtonClick()));
    connect(ui->btnRemoveAll, SIGNAL(clicked()), this, SLOT(onRemoveAllButtonClick()));
    connect(ui->buttonBox, SIGNAL(accepted()), this, SLOT(onAccepted()));
    connect(ui->buttonBox, SIGNAL(rejected()), this, SLOT(onRejected()));
    connect(ui->btnMoveUp, SIGNAL(clicked()), this, SLOT(onMoveUpClick()));
    connect(ui->btnMoveDown, SIGNAL(clicked()), this, SLOT(onMoveDownClick()));

    ui->btnAdd->setEnabled(false);
    ui->btnRemove->setEnabled(false);
    ui->btnMoveUp->setEnabled(false);
    ui->btnMoveDown->setEnabled(false);
}

FormatSelectorDialog::~FormatSelectorDialog()
{
    delete ui;
}

void FormatSelectorDialog::setModel(QAbstractItemModel *newModel)
{
    if (newModel && _model != newModel)
    {
        if (_model != Q_NULLPTR)
        {
            disconnect(ui->listSelected->selectionModel(), SIGNAL(selectionChanged(QItemSelection,QItemSelection)),
                    this, SLOT(onSelectedSelectionChanged(QItemSelection,QItemSelection)));
            disconnect(ui->listAvailable->selectionModel(), SIGNAL(selectionChanged(QItemSelection,QItemSelection)),
                       this, SLOT(onAvailableSelectionChanged(QItemSelection,QItemSelection)));
        }

        _model = newModel;
        resetSelector();

        connect(ui->listSelected->selectionModel(), SIGNAL(selectionChanged(QItemSelection,QItemSelection)),
                this, SLOT(onSelectedSelectionChanged(QItemSelection,QItemSelection)));
        connect(ui->listAvailable->selectionModel(), SIGNAL(selectionChanged(QItemSelection,QItemSelection)),
                   this, SLOT(onAvailableSelectionChanged(QItemSelection,QItemSelection)));

        checkButtonsState();
    }
}

bool FormatSelectorDialog::displayHeaders() const
{
    return ui->chDisplayHeaders->isChecked();
}

void FormatSelectorDialog::onAvailableDoubleClick(const QModelIndex &index)
{
    if (!index.isValid() || !_model)
        return;

    _availableProxy->setData(index, true, InscriptionTreeModel::TemporarySelectedRole);
    checkButtonsState();
}

void FormatSelectorDialog::onSelectedDoubleClick(const QModelIndex &index)
{
    if (!index.isValid() || !_model)
        return;

    _selectedProxy->setData(index, false, InscriptionTreeModel::TemporarySelectedRole);
    checkButtonsState();
}

void FormatSelectorDialog::onAddButtonClick()
{
    QModelIndexList selectedItems = ui->listAvailable->selectionModel()->selectedIndexes();
    QVector<int> selectedRows;
    foreach (QModelIndex index, selectedItems) {
        auto indexInModel = _availableProxy->mapToSource(index);
        selectedRows.push_back(indexInModel.row());
    }
    foreach (int row, selectedRows) {
       _model->setData(_model->index(row, 0), true, InscriptionTreeModel::TemporarySelectedRole);
    }
    checkButtonsState();
}

void FormatSelectorDialog::onAddAllButtonClick()
{
    ui->listAvailable->selectAll();
    onAddButtonClick();
}

void FormatSelectorDialog::onRemoveButtonClick()
{
    QModelIndexList selectedItems = ui->listSelected->selectionModel()->selectedIndexes();
    QVector<int> rows;
    foreach (QModelIndex index, selectedItems) {
        auto indexInModel = _selectedProxy->mapToSource(index);
        rows.push_back(indexInModel.row());
    }
    foreach (int row, rows) {
       _model->setData(_model->index(row, 0), false, InscriptionTreeModel::TemporarySelectedRole);
    }
    checkButtonsState();
}

void FormatSelectorDialog::onRemoveAllButtonClick()
{
    ui->listSelected->selectAll();
    onRemoveButtonClick();
}

void FormatSelectorDialog::onAccepted()
{
    // commit changes: copy all data from UserRole+2 -> UserRole+1
    for (int i = 0; i < _model->rowCount(); i++)
    {
        QModelIndex ind = _model->index(i, 0);
        QVariant value = _model->data(ind, InscriptionTreeModel::TemporarySelectedRole);
        _model->setData(ind, value, InscriptionTreeModel::SelectedRole);
    }
    QModelIndex ind = _model->index(0, 0);
    _model->setData(ind, ui->chDisplayHeaders->isChecked(), InscriptionTreeModel::ShowColumnNameRole);
}

void FormatSelectorDialog::onRejected()
{
    for (int i = 0; i < _model->rowCount(); i++)
    {
        QModelIndex ind = _model->index(i, 0);
        QVariant value = _model->data(ind, InscriptionTreeModel::SelectedRole);
        _model->setData(ind, value, InscriptionTreeModel::TemporarySelectedRole);
    }
}

void FormatSelectorDialog::onMoveUpClick()
{
    moveSelectedItem(-1);
}

void FormatSelectorDialog::onMoveDownClick()
{
    moveSelectedItem(1);
}

void FormatSelectorDialog::moveSelectedItem(int offset) const
{
    auto selectedIndexes = ui->listSelected->selectionModel()->selectedRows();
    if (selectedIndexes.isEmpty())
        return;
    if (offset != 1 && offset != -1)
        return;

    auto row = selectedIndexes[0].row();
    if (row == 0 && offset == -1)
        return;
    if (row == ui->listSelected->model()->rowCount()-1 && offset == 1)
        return;

    auto modelIndex = _selectedProxy->mapToSource(selectedIndexes[0]);
    auto model = qobject_cast<QStandardItemModel*>(_model);
    row = modelIndex.row();

    auto movedItem = model->itemFromIndex(modelIndex);

    model->takeRow(row);
    model->insertRow(row + offset, movedItem);
    auto newSelection = ui->listSelected->model()->index(selectedIndexes[0].row() + offset, 0);
    ui->listSelected->selectionModel()->select(newSelection, QItemSelectionModel::SelectCurrent);
}

void FormatSelectorDialog::onSelectedSelectionChanged(const QItemSelection &selected, const QItemSelection &deselected)
{
    Q_UNUSED(deselected)
    ui->btnMoveUp->setEnabled(!selected.indexes().isEmpty());
    ui->btnMoveDown->setEnabled(!selected.indexes().isEmpty());

    if (selected.indexes().size() == 1 && selected.indexes().at(0).row() == 0)
        ui->btnMoveUp->setEnabled(false);
    if (selected.indexes().size() == 1 && selected.indexes().at(0).row() == ui->listSelected->model()->rowCount()-1)
        ui->btnMoveDown->setEnabled(false);

    checkButtonsState();
}

void FormatSelectorDialog::onAvailableSelectionChanged(const QItemSelection &selected, const QItemSelection &deselected)
{
    Q_UNUSED(selected)
    Q_UNUSED(deselected)
    checkButtonsState();
}

void FormatSelectorDialog::resetSelector()
{
    if (!_model)
    {
        return;
    }

    _availableProxy->setSourceModel(_model);
    _selectedProxy->setSourceModel(_model);
    ui->listAvailable->setModel(_availableProxy);
    ui->listSelected->setModel(_selectedProxy);
    _availableProxy->sort(0);

    QModelIndex ind = _model->index(0, 0);
    bool showHeaders = _model->data(ind, InscriptionTreeModel::ShowColumnNameRole).toBool();
    ui->chDisplayHeaders->setChecked(showHeaders);
}

void FormatSelectorDialog::checkButtonsState()
{
    if (_model == Q_NULLPTR)
    {
        QList<QWidget*> widgets;
        widgets << ui->btnAdd << ui->btnAddAll << ui->btnRemove << ui->btnRemoveAll
                << ui->buttonBox->button(QDialogButtonBox::Ok)
                << ui->btnMoveUp << ui->btnMoveDown;
        foreach (auto widget, widgets) {
           widget->setEnabled(false);
        }
        return;
    }
    ui->btnAdd->setEnabled(ui->listAvailable->selectionModel()->selectedRows().size() > 0);
    ui->btnAddAll->setEnabled(ui->listAvailable->model()->rowCount() != 0);

    ui->btnRemove->setEnabled(ui->listSelected->selectionModel()->selectedRows().size() > 0);
    ui->btnRemoveAll->setEnabled(ui->listSelected->model()->rowCount() != 0);

    bool okAvailable = true;
    if (ui->listSelected->model()->rowCount() == 0)
    {
        // nothing is selected
        okAvailable = false;
    }
    ui->buttonBox->button(QDialogButtonBox::Ok)->setEnabled(okAvailable);
    _availableProxy->sort(0);
}
