//
// This file is part of Rundata-qt.
//
// This program is free software licensed under the GNU GPL v3 or any later version.
// You can find a copy of this license in LICENSE.GPLv3 in the top directory of
// the source code.
//
// Copyright 2017 Vadim Frolov <fralik@gmail.com>
//
#include "FilterItem.h"
#include "FilterModel.h"

#include <QDebug>
#include <QVector>
#include <QVariant>

FilterModel::FilterModel(const QStringList &headers, const QString &data, QObject *parent)
    : QAbstractItemModel(parent)
{
    QVector<QVariant> rootData;
    foreach (QString header, headers) {
        rootData << header;
    }

    _rootItem = new FilterItem(rootData);

    fromString(data);
}

FilterModel::~FilterModel()
{
    delete _rootItem;
}

QVariant FilterModel::data(const QModelIndex &index, int role) const
{
    if (!index.isValid())
        return QVariant();

    if (role != Qt::DisplayRole && role != Qt::EditRole)
        return QVariant();

    auto item = _getItem(index);
    // We should not show operator for the very first filter item only
    // However show it if NOT is present
    if (index.row() == 0 && index.column() == 0 && !parent(index).isValid()
            && role == Qt::DisplayRole) {
        if (item->data(index.column()).toString().contains(QStringLiteral("NOT"), Qt::CaseInsensitive))
        {
            return QVariant(QStringLiteral("NOT"));
        }
        return QVariant();
    }
    return item->data(index.column());
}

QVariant FilterModel::headerData(int section, Qt::Orientation orientation, int role) const
{
    if (orientation == Qt::Horizontal && role == Qt::DisplayRole)
        return _rootItem->data(section);

    return QVariant();
}

QModelIndex FilterModel::index(int row, int column, const QModelIndex &parent) const
{
    if (parent.isValid() && parent.column() != 0)
        return QModelIndex();

    auto parentItem = _getItem(parent);
    auto childItem = parentItem->child(row);
    if (childItem)
        return createIndex(row, column, childItem);
    else
        return QModelIndex();
}

QModelIndex FilterModel::parent(const QModelIndex &index) const
{
    if (!index.isValid())
        return QModelIndex();

    auto childItem = _getItem(index);
    auto parentItem = childItem->parent();

    if (parentItem == _rootItem)
        return QModelIndex();

    return createIndex(parentItem->row(), 0, parentItem);
}

int FilterModel::rowCount(const QModelIndex &parent) const
{
    auto parentItem = _getItem(parent);

    return parentItem->childCount();
}

int FilterModel::columnCount(const QModelIndex &parent) const
{
    Q_UNUSED(parent);
    return _rootItem->columnCount();
}

bool FilterModel::setData(const QModelIndex &index, const QVariant &value, int role)
{
    if (role != Qt::EditRole)
        return false;

    auto item = _getItem(index);
    bool result = item->setData(index.column(), value);
    qDebug() << "FilterModel::setData, index (" << index.row() << ", " << index.column() << "), value" << value << "result:" << result;

    if (result)
        emit dataChanged(index, index);

    return result;
}

bool FilterModel::setHeaderData(int section, Qt::Orientation orientation, const QVariant &value, int role)
{
    if (role != Qt::EditRole || orientation != Qt::Horizontal)
        return false;

    bool result = _rootItem->setData(section, value);

    if (result)
        emit headerDataChanged(orientation, section, section);

    return result;
}

bool FilterModel::insertRows(int row, int count, const QModelIndex &parent)
{
    auto parentItem = _getItem(parent);
    bool success;

    beginInsertRows(parent, row, row + count - 1);
    success = parentItem->insertChildren(row, count, _rootItem->columnCount());
    endInsertRows();

    return success;
}

bool FilterModel::insertColumns(int column, int count, const QModelIndex &parent)
{
    bool success;

    beginInsertColumns(parent, column, column + count - 1);
    success = _rootItem->insertColumns(column, count);
    endInsertColumns();

    return success;
}

bool FilterModel::removeRows(int row, int count, const QModelIndex &parent)
{
    auto parentItem = _getItem(parent);
    bool success = true;

    beginRemoveRows(parent, row, row + count - 1);
    success = parentItem->removeChildren(row, count);
    endRemoveRows();

    return success;
}

bool FilterModel::removeColumns(int column, int count, const QModelIndex &parent)
{
    bool success;

    beginRemoveColumns(parent, column, column + count - 1);
    success = _rootItem->removeColumns(column, count);
    endRemoveColumns();

    if (_rootItem->columnCount() == 0)
        removeRows(0, rowCount());

    return success;
}

QString FilterModel::toString() const
{
    QModelIndex index;
    int numRows = rowCount(index);
    if (numRows == 0)
        return QString();

    QString res;
    QStringList rows;
    QList<FilterItem*> parents;
    QList<int> levels;

    parents << _rootItem;
    levels << -1;

    while (!parents.isEmpty())
    {
        auto curNode = parents.takeLast();
        auto level = levels.takeLast();

        QString rowItems = curNode->toString();
        if (level > 0)
        {
            QString levelOffset;
            for (int i = 0; i < level; ++i)
                levelOffset.append(QLatin1Char(' '));
            levelOffset.append(QLatin1Char('\t'));
            rowItems = levelOffset + rowItems;
        }

        rows << rowItems;

        for (int i = curNode->childCount()-1; i >= 0; --i)
        {
            parents << curNode->child(i);
            levels << level + 1;
        }
    }

    rows.push_front(QStringLiteral("Name: searchParameters; Version: %1.%2").arg(1).arg(0));
    res = rows.join(QLatin1Char('\n'));
    return res;
}

void FilterModel::fromString(const QString &str)
{
    if (str.isEmpty())
        return;

    int number = 0;
    const QStringList lines = str.split(QLatin1Char('\n'), QString::SkipEmptyParts);
    if (lines[0].contains(QLatin1String("Version")))
    {
        if (_rootItem != Q_NULLPTR)
        {
            if (rowCount() > 0)
                removeRows(0, rowCount());

            delete _rootItem;
            _rootItem = Q_NULLPTR;
        }
    }
    else
    {
        // handle old version of toString function
        if (_rootItem == Q_NULLPTR)
            return;
    }
    if (_rootItem == Q_NULLPTR)
    {
        QVector<QVariant> rootData;
        QStringList columnStrings = lines[1].split(QLatin1Char('\t'), QString::SkipEmptyParts);
        foreach (auto header, columnStrings) {
            rootData << header;
        }
        _rootItem = new FilterItem(rootData);
        number = 2;
    }

    QList<int> indentations;
    indentations << 0;
    QList<FilterItem*> parents;
    parents << _rootItem;

    const QModelIndex rootIndex = index(0, 0);
    beginInsertRows(rootIndex, 1, lines.count()-number);

    while (number < lines.count())
    {
        int position = 0;
        while (position < lines[number].length())
        {
            if (lines[number].at(position) != QLatin1Char(' '))
                break;
            ++position;
        }
        QString lineData = lines[number].mid(position).trimmed();

        if (!lineData.isEmpty())
        {
            // Read the column data from the rest of the line.
            QStringList columnStrings = lineData.split(QLatin1Char('\t'), QString::KeepEmptyParts);
            QVector<QVariant> columnData;
            for (int column = 0; column < columnStrings.count(); ++column)
                columnData << columnStrings[column];

            if (position > indentations.last()) {
                // The last child of the current parent is now the new parent
                // unless the current parent has no children.

                if (parents.last()->childCount() > 0) {
                    parents << parents.last()->child(parents.last()->childCount()-1);
                    indentations << position;
                }
            } else {
                while (position < indentations.last() && parents.count() > 0) {
                    parents.pop_back();
                    indentations.pop_back();
                }
            }

            // Append a new item to the current parent's list of children.
            auto parent = parents.last();
            parent->insertChildren(parent->childCount(), 1, _rootItem->columnCount());
            for (int column = 0; column < columnData.size(); ++column)
                parent->child(parent->childCount() - 1)->setData(column, columnData[column]);
        }

        ++number;
    }

    endInsertRows();
}

FilterItem *FilterModel::_getItem(const QModelIndex &index) const
{
    if (index.isValid())
    {
        auto item = static_cast<FilterItem*>(index.internalPointer());
        if (item)
            return item;
    }
    return _rootItem;
}

Qt::ItemFlags FilterModel::flags(const QModelIndex &index) const
{
    Qt::ItemFlags defaultFlags = QAbstractItemModel::flags(index);
    if (index.isValid())
        return Qt::ItemIsDragEnabled | Qt::ItemIsDropEnabled | defaultFlags;
    else
        return Qt::ItemIsDropEnabled | defaultFlags;
}

// This implementation doesn't preserve children of moved item!
// If we drag an item with children, there could be several situations:
// 1. item is dropped on a root
// 2. item is dropped on a top-level item (i.e. parent of that item is root).
// 3. item is dropped on child item. It is not really clear what is the best way of action in that case.
//    One should either not allow such drop, or reassign dragged item's children to be children of the dropped item.
//
bool FilterModel::moveRows(const QModelIndex &sourceParent, int sourceRow, int count, const QModelIndex &destinationParent, int destinationChild)
{
    bool result = true;

    auto sourceParentItem = _getItem(sourceParent);
    for (int i = sourceRow; i < sourceRow + count; ++i)
    {
        int destinationOffset = i - sourceRow;

        auto rowToMove = sourceParentItem->child(i);
        QVariantList columnData;
        for (int i = 0; i < rowToMove->columnCount(); ++i)
            columnData << rowToMove->data(i);
        result = result & removeRow(sourceRow, sourceParent);

        auto parent = _getItem(destinationParent);
        insertRow(destinationChild + destinationOffset, destinationParent);
        for (int column = 0; column < columnData.size(); ++column)
            result = result & parent->child(destinationChild + destinationOffset)->setData(column, columnData[column]);
    }

    return result;
}


Qt::DropActions FilterModel::supportedDropActions() const
{
    return Qt::MoveAction;
}

Qt::DropActions FilterModel::supportedDragActions() const
{
    return Qt::MoveAction;
}
