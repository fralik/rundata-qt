<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="sv_SE">
<context>
    <name>FormatSelector</name>
    <message>
        <location filename="../format_selector.ui" line="14"/>
        <source>Field selection</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../format_selector.ui" line="20"/>
        <source>Select what information will be shown for each inscription. Use mouse to rearrange the order.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../format_selector.ui" line="27"/>
        <source>Selected:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../format_selector.ui" line="34"/>
        <source>Available information fields:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../format_selector.ui" line="59"/>
        <source>Select if you want to see data description, omit if you only interested in data</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../format_selector.ui" line="62"/>
        <source>Display headers</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../format_selector.ui" line="97"/>
        <source>Move selected field up</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../format_selector.ui" line="100"/>
        <source>↑</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../format_selector.ui" line="113"/>
        <source>Move selected field down</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../format_selector.ui" line="116"/>
        <source>↓</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../format_selector.ui" line="146"/>
        <source>Add all</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../format_selector.ui" line="149"/>
        <source>⇒</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../format_selector.ui" line="162"/>
        <source>Add selected</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../format_selector.ui" line="168"/>
        <source>→</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../format_selector.ui" line="181"/>
        <source>Remove selected</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../format_selector.ui" line="184"/>
        <source>←</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../format_selector.ui" line="200"/>
        <source>Remove all</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../format_selector.ui" line="203"/>
        <source>⇐</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>PanelWindow</name>
    <message>
        <location filename="../panelwindow.ui" line="14"/>
        <source>RunData Qt</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../panelwindow.ui" line="122"/>
        <source>Apply filter(s)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../panelwindow.ui" line="166"/>
        <source>earth/openstreetmap/openstreetmap.dgml</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../panelwindow.ui" line="217"/>
        <source>&amp;Search parameters</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../panelwindow.ui" line="228"/>
        <source>&amp;Format</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../panelwindow.ui" line="234"/>
        <source>&amp;View</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../panelwindow.ui" line="240"/>
        <source>&amp;Help</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../panelwindow.ui" line="255"/>
        <source>&amp;Edit current parameter</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../panelwindow.ui" line="260"/>
        <source>&amp;Remove current parameter</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../panelwindow.ui" line="265"/>
        <source>Add &amp;subparameter</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../panelwindow.ui" line="270"/>
        <source>Add new &amp;parameter</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../panelwindow.ui" line="275"/>
        <source>Displayed &amp;fields</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../panelwindow.ui" line="278"/>
        <source>Select what is shown about each inscription</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../panelwindow.ui" line="283"/>
        <source>&amp;Clear all</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../panelwindow.ui" line="286"/>
        <source>Clears all parameters</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../panelwindow.ui" line="291"/>
        <source>Reload style</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../panelwindow.ui" line="302"/>
        <source>Show &amp;map</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../panelwindow.ui" line="310"/>
        <source>&amp;About</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../panelwindow.ui" line="315"/>
        <source>About &amp;Qt</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../panelwindow.ui" line="318"/>
        <source>Show information about the Qt library</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../panelwindow.ui" line="326"/>
        <source>User manual</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../panelwindow.ui" line="331"/>
        <source>Licenses</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../panelwindow.cpp" line="87"/>
        <source>Operator</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../panelwindow.cpp" line="87"/>
        <source>Parameter</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../panelwindow.cpp" line="87"/>
        <source>Value</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../panelwindow.cpp" line="87"/>
        <source>column name</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../panelwindow.cpp" line="160"/>
        <source>Ctrl+E</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../panelwindow.cpp" line="161"/>
        <source>Ctrl+M</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../panelwindow.cpp" line="752"/>
        <source>Sides:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../panelwindow.cpp" line="754"/>
        <source>Reading variants:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../panelwindow.cpp" line="781"/>
        <source>Contains %1 cross(es)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../panelwindow.cpp" line="889"/>
        <source>Any file (*.css)</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>SearchColumnDialog</name>
    <message>
        <location filename="../searchcolumndialog.ui" line="20"/>
        <source>Search parameter selection</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../searchcolumndialog.ui" line="26"/>
        <source>What are you searching for?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../searchcolumndialog.ui" line="43"/>
        <source>AND</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../searchcolumndialog.ui" line="56"/>
        <source>OR</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../searchcolumndialog.ui" line="66"/>
        <source>NOT</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>SignatureTreeModel</name>
    <message>
        <location filename="../SignatureTreeModel.cpp" line="25"/>
        <source>Signature</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../SignatureTreeModel.cpp" line="26"/>
        <source>Found location</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../SignatureTreeModel.cpp" line="27"/>
        <source>Parish</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../SignatureTreeModel.cpp" line="28"/>
        <source>District</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../SignatureTreeModel.cpp" line="29"/>
        <source>Municipality</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../SignatureTreeModel.cpp" line="30"/>
        <source>Current location</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../SignatureTreeModel.cpp" line="31"/>
        <source>Original site</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../SignatureTreeModel.cpp" line="32"/>
        <source>Rune type</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../SignatureTreeModel.cpp" line="33"/>
        <source>Dating</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../SignatureTreeModel.cpp" line="34"/>
        <source>Style</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../SignatureTreeModel.cpp" line="35"/>
        <source>Carver</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../SignatureTreeModel.cpp" line="36"/>
        <source>Material type</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../SignatureTreeModel.cpp" line="37"/>
        <source>Material</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../SignatureTreeModel.cpp" line="38"/>
        <source>Object</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../SignatureTreeModel.cpp" line="39"/>
        <source>References</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../SignatureTreeModel.cpp" line="40"/>
        <source>Normalisation to Old West Norse</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../SignatureTreeModel.cpp" line="41"/>
        <source>Normalisation to Old Scandinavian</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../SignatureTreeModel.cpp" line="42"/>
        <source>Translation to English</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../SignatureTreeModel.cpp" line="43"/>
        <source>Transliterated runic text</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../SignatureTreeModel.cpp" line="44"/>
        <source>Cross form</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../SignatureTreeModel.cpp" line="45"/>
        <source>Number of crosses</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../SignatureTreeModel.cpp" line="46"/>
        <source>Other</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../SignatureTreeModel.cpp" line="47"/>
        <source>Alternative signature?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../SignatureTreeModel.cpp" line="48"/>
        <source>Lost?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../SignatureTreeModel.cpp" line="49"/>
        <source>New reading?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../SignatureTreeModel.cpp" line="55"/>
        <source>Image link</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../SignatureTreeModel.cpp" line="231"/>
        <source>Signature is lost and has a new reading</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../SignatureTreeModel.cpp" line="233"/>
        <source>Signature is lost</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../SignatureTreeModel.cpp" line="235"/>
        <source>Signature has a new reading</source>
        <translation type="unfinished"></translation>
    </message>
</context>
</TS>
